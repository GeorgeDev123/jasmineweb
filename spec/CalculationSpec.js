// describe("Calculation", function(){
//     beforeEach(function() {
//         Calculation.property.streetNo = 1;
//         Calculation.property1.streetNo = 2;
//         Calculation.matter = {
//         propertyList: [Calculation.property, Calculation.property1]
//         };
//         Calculation.volume =  "13245";
//         Calculation.folio = "123";
//         Calculation.state = "NSW";
//         Calculation.titleReference = "Title reference";
//         Calculation.book = "book";
//     });

//     it("should return properties", function(){
//         expect(Calculation.property).toBeDefined();
//         expect(Calculation.property1).toBeDefined();
//         expect(Calculation.matter).toBeDefined();
//         expect(Calculation.matter.propertyList[1].streetNo).toBeDefined();
//         expect(Calculation.returnProperties()).toEqual("properties");
//     });
//     it("should return a property", function(){
//         Calculation.property1.streetNo = undefined;    
//         expect(Calculation.property).toBeDefined();
//         expect(Calculation.property1).toBeDefined();
//         expect(Calculation.matter).toBeDefined();
//         expect(Calculation.matter.propertyList[1].streetNo).toBeUndefined();
//         expect(Calculation.returnProperties()).toEqual("a property");
//     });
//     it("Given volume of 13245 and folio of 123 actVolume should return 13245:123", function(){
//         expect(Calculation.volume).toBeDefined();
//         expect(Calculation.folio).toBeDefined();
//         expect(Calculation.actVolume()).toEqual("13245:123");
//     });
//     it("Given volume is Defined volFolConveyBkSingle should return Volume13245Folio123",function(){
//         expect(Calculation.volume).toBeDefined();
//         expect(Calculation.folio).toBeDefined();
//         expect(Calculation.volFolConveyBkSingle()).toEqual("Volume13245Folio123");          
//     });
//     it("Given volume is Undefined volFolConveyBkSingle should return Conveyance No. 123 Book book",function(){
//         Calculation.volume = undefined;
//         expect(Calculation.volume).toBeUndefined();
//         expect(Calculation.conveyanceNo).toBeDefined();
//         expect(Calculation.volFolConveyBkSingle()).toEqual("Conveyance No. 123 Book book");          
//     });
//     it("Given volume of 13245 and folio of 123 actVolume should return Volume 13245 Folio 123",function(){
//         expect(Calculation.volume).toBeDefined();
//         expect(Calculation.conveyanceNo).toBeDefined();
//         expect(Calculation.volumeAndFolio()).toEqual("Volume 13245 Folio 123");    
//         expect(Calculation).toBeDefined();
//         expect(Card).toBeDefined();
//     });
// })
describe("Admin Tribunal Forms Calculation", function(){
    it("Given right amount it should return 33",function(){
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.AmtPerFtOtherIncomeTotal()).toEqual("$33.00");    
    });
    it("Given Wed Jul 28 1993 14:39:07 GMT-0600 (PDT) it should return Wed Jul 28 1993",function(){
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.DisplayAdt()).toEqual("Date of Decision: 28 July, 1993");    
    });    
    it("Given right amount for totalAmtPerFtOtherIncome it should return 33",function(){
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.totalAmtPerFtOtherIncome()).toEqual("$33.00");    
    });        
    it("Given rentBoardMortgage creditCardHpLoanRepayments livingExpFoodClothingEtc expFaresGasElecEtc otherPerFtAmt1 otherPerFtAmt2 equal to 10 it should return 50",function(){
        expect(Tribunal.rentBoardMortgage).toBeDefined();
        expect(Tribunal.creditCardHpLoanRepayments).toBeDefined();
        expect(Tribunal.livingExpFoodClothingEtc).toBeDefined();
        expect(Tribunal.expFaresGasElecEtc).toBeDefined();
        expect(Tribunal.otherPerFtAmt1).toBeDefined();
        expect(Tribunal.otherPerFtAmt2).toBeDefined();
        expect(Tribunal.TotalExp()).toEqual("$60.00");    
    });
    it("Given right amount for NetIncomePerFortnight it should return 48",function(){
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.netIncomePerFortnight).toBeDefined();
        expect(Tribunal.NetIncomePerFortnight()).toEqual("$48.00");
    });
    it("Given right amount for NetIncomePerFortnight it should return 48",function(){
        expect(Tribunal.amtPerFortnight1).toBeDefined();
        expect(Tribunal.netIncomePerFortnight).toBeDefined();
        expect(Tribunal.NetIncomePerFortnight()).toEqual("$48.00");
    });
});

describe("Cars Forms Calculation", function(){
    beforeEach(function() {
        Cars.carsWhichFormAreYouCompleting = "2A";
        Cars.cars2rIsQ5TimeLimitInformationCorrect = "No";
        Cars.carsDateClaimFormSentToInsurer = new Date(2012, 6, 28, 14, 39, 7);
        //Client.FileName = "Defendant";
        Client.FileName = "";
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = "1";
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "1";
        Cars.cars2a5bHowIsS912bSatisfied = "1";
        Cars.cars2a5bHowIsS912aSatisfied = "1";
        Cars.cars2a5bHowIsS912aSatisfied = "1";
        Cars.cars2rIsQ6InformationCorrect = "Yes";
        Cars.cars2a6aIsClaimNotSuitableForAssessment = "Yes";
        Cars.cars2a6bReasonClaimIsNotReadyForAssessment = "1";
        Cars.cars2a6bWhenWillClaimBeReadyForAssessment = "01/12/2016";
        Cars.cars2a6bIsClaimReadyForAssessment = "Yes";
        Cars.cars2rIsQ7InformationCorrect = "Yes";
        Cars.cars2a7ReasonForLiabilityIssue = "1";
        Cars.cars2a7IsLiabilityAnIssue = "Yes";
        Cars.cars2a71DoesClaimantAgreeTheyAreAtFault = "Yes";
        Cars.carsReplyIsDisputeInformationCorrect = "Yes";
        Cars.cars2a8aWhatIsAgreedForNoneconomicLoss = "1";
        Cars.cars2a8aHowIsCl93Satisfied = "1";
        Cars.cars2a5bDetails = "cars2a5bDetails";
        Cars.cars2r8aReplyIsInfoCorrect = "1";
        Cars.cars5aQ6aDisputeInformation = "A";
        Cars.cars5aQ6bDisputeInformation = "A";
        Cars.carsWhoIsTheClaimMadeAgainst = "1";
    });
    it("Given carsWhichFormAreYouCompleting is 2A",function(){
        expect(Cars.carsWhichFormAreYouCompleting).toBeDefined();
        expect(Cars.carsWhichFormSelection()).toEqual("2A");    
    });
    it("Given carsWhichFormAreYouCompleting is 2A then carsFlagtextTimeLimitInformation should return yes",function(){
        expect(Cars.carsWhichFormAreYouCompleting).toBeDefined();
        expect(Cars.carsFlagtextTimeLimitInformation()).toEqual("Yes");    
    });
    it("Given carsWhichFormAreYouCompleting is 2R and cars2rIsQ5TimeLimitInformationCorrect is false then carsFlagtextTimeLimitInformation should return yes",function(){
        expect(Cars.carsWhichFormAreYouCompleting).toBeDefined();
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2rIsQ5TimeLimitInformationCorrect).toBeDefined();
        expect(Cars.carsFlagtextTimeLimitInformation()).toEqual("Yes");    
    });    
    it("Given carsWhichFormAreYouCompleting is 2R and cars2rIsQ5TimeLimitInformationCorrect is true then carsFlagtextTimeLimitInformation should return no",function(){
        expect(Cars.carsWhichFormAreYouCompleting).toBeDefined();
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2rIsQ5TimeLimitInformationCorrect).toBeDefined();
        Cars.cars2rIsQ5TimeLimitInformationCorrect = "Yes"
        expect(Cars.carsFlagtextTimeLimitInformation()).toEqual("No");
    });
    it("Given Client FileName is Defendant then carsActingForClaimantOrInsurer should return Insurer",function(){
        Client.FileName = "Defendant";
        expect(Client.FileName).toBeDefined();
        expect(Cars.carsActingForClaimantOrInsurer()).toEqual("Insurer");
    });
    it("Given Client FileName is Undefined then carsActingForClaimantOrInsurer should return Claimant",function(){
        Client.FileName = undefined;
        expect(Client.FileName).toBeUndefined();
        expect(Cars.carsActingForClaimantOrInsurer()).toEqual("Claimant");
    });
    it("Given carsActingForClaimantOrInsurer Claimant and carDateClaimFormSentAfter is 2012 then carsWasTheClaimMadeOnOrAfter1October2008 should return Yes",function(){
        expect(Cars.carsDateClaimFormSentToInsurer).toBeDefined();
        expect(Cars.carsWasTheClaimMadeOnOrAfter1October2008()).toEqual("Yes");
    });    
    it("Given carsActingForClaimantOrInsurer Claimant and carDateClaimFormSentAfter is 2002 then carsWasTheClaimMadeOnOrAfter1October2008 should return No",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2002, 6, 28, 14, 39, 7)
        expect(Cars.carsDateClaimFormSentToInsurer).toBeDefined();
        expect(Cars.carsWasTheClaimMadeOnOrAfter1October2008()).toEqual("No");
    });
    it("Given cars2a5aHowIsS91TimeLimitsSatisfied is 1 then cars2a5aS91TimeLimitsSelection should return 1",function(){
        expect(Cars.cars2a5aHowIsS91TimeLimitsSatisfied).toBeDefined();
        expect(Cars.cars2a5aS91TimeLimitsSelection()).toEqual("1");
    });    
    it("Given cars2a5aHowIsS91TimeLimitsSatisfied is undefined then cars2a5aS91TimeLimitsSelection should return empty",function(){
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = undefined;
        expect(Cars.cars2a5aHowIsS91TimeLimitsSatisfied).toBeUndefined();
        expect(Cars.cars2a5aS91TimeLimitsSelection()).toEqual("");
    });
    it("Given cars2a5aHowIsS91TimeLimitsSatisfied is empty then cars2a5aS91TimeLimitsSelection should return empty",function(){
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = null;
        expect(Cars.cars2a5aS91TimeLimitsSelection()).toEqual("");
    });    
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return yes, and cars2a5aS91TimeLimitsSelection is 1 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2000, 6, 28, 14, 39, 7);
        expect(Cars.cars2a5aFlag1S911a()).toEqual("<<");
    });        
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return yes, and cars2a5aS91TimeLimitsSelection is 2 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2000, 6, 28, 14, 39, 7);
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = "2";
        expect(Cars.cars2a5aFlag2S911b()).toEqual("<<");
    });    
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return yes, and cars2a5aS91TimeLimitsSelection is 3 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2000, 6, 28, 14, 39, 7);
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = "3";
        expect(Cars.cars2a5aFlag3S912()).toEqual("<<");
    });
    it("Given cars2a5aHowIsS912Satisfied is 1 then cars2a5aS91TimeLimitsSelection should return 1",function(){
        expect(Cars.cars2a5aHowIsS91TimeLimitsSatisfied).toBeDefined();
        expect(Cars.cars2a5aS912Selection()).toEqual("1");
    });      
    it("Given cars2a5bHowIsS91TimeLimitsSatisfied is 1 then cars2a5bS91TimeLimitsSelection should return 1",function(){
        expect(Cars.cars2a5aHowIsS91TimeLimitsSatisfied).toBeDefined();
        expect(Cars.cars2a5bS91TimeLimitsSelection()).toEqual("1");
    });          
    it("Given cars2a5bHowIsS912bSatisfied is 1 then cars2a5bS912bSelection should return 1",function(){
        expect(Cars.cars2a5aHowIsS91TimeLimitsSatisfied).toBeDefined();
        expect(Cars.cars2a5bS912bSelection()).toEqual("1");
    }); 
    it("Given cars2a5bS91TimeLimitsSelection return 1 then cars2a5bDetailsLabel should return Not applicable",function(){
        expect(Cars.cars2a5bDetailsLabel()).toEqual("Not applicable");
    }); 
    it("Given cars2a5bS91TimeLimitsSelection return 3 and cars2a5bS912bSelection return 1 then cars2a5bDetailsLabel should return Enter details of how s89A(3) applies",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3";
        Cars.cars2a5bHowIsS912bSatisfied = "1";
        expect(Cars.cars2a5bDetailsLabel()).toEqual("Enter details of how s89A(3) applies");
    }); 
    it("Given cars2a5bS91TimeLimitsSelection return 3 and cars2a5bS912bSelection return 2 then cars2a5bDetailsLabel should return Enter details of how s89C(4) applies",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3";
        Cars.cars2a5bHowIsS912bSatisfied = "2";
        expect(Cars.cars2a5bDetailsLabel()).toEqual("Enter details of how s89C(4) applies");
    }); 
    it("Given cars2a5bS91TimeLimitsSelection return 3 and cars2a5bS912bSelection return 3 then cars2a5bDetailsLabel should return empty",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3";
        Cars.cars2a5bHowIsS912bSatisfied = "3";
        expect(Cars.cars2a5bDetailsLabel()).toEqual("");
    }); 
    it("Given cars2a5bS91TimeLimitsSelection return 5 and cars2a5bS912bSelection return 3 then cars2a5bDetailsLabel should return Enter details of Enter details of how s91(2)(d) is satisfied",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "5";
        Cars.cars2a5bHowIsS912bSatisfied = "3";
        expect(Cars.cars2a5bDetailsLabel()).toEqual("Enter details of how s91(2)(d) is satisfied");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 1 then cars2a5aFlag1S911a should return empty",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        expect(Cars.cars2a5bFlag1S911()).toEqual("<<");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return No, and cars2a5aS91TimeLimitsSelection is 1 then cars2a5aFlag1S911a should return empty",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2000, 6, 28, 14, 39, 7);
        expect(Cars.cars2a5bFlag1S911()).toEqual("");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return No, cars2a5bHowIsS91TimeLimitsSatisfied is 1 and cars2a5bHowIsS912aSatisfied is 1,  then cars2a5bFlag2S89eb should return empty",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2000, 6, 28, 14, 39, 7);
        Cars.cars2a5aHowIsS91TimeLimitsSatisfied = "3";
        expect(Cars.cars2a5bFlag2S89eb()).toEqual("");
    });               
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return No, and cars2a5aS91TimeLimitsSelection is 2 then cars2a5bFlag2S89eb should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS912aSatisfied = "2"
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "2";
        expect(Cars.cars2a5bFlag2S89eb()).toEqual("<<");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 2 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "2"
        expect(Cars.cars2a5bFlag2S912a()).toEqual("<<");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 3 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3"
        expect(Cars.cars2a5bFlag3S912b()).toEqual("<<");
    }); 
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 4 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "4"
        expect(Cars.cars2a5bFlag4S912c()).toEqual("<<");
    }); 
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 5 then cars2a5aFlag1S911a should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "5"
        expect(Cars.cars2a5bFlag5S912d()).toEqual("<<");
    });            
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 5 then cars2a5bFlagDetails should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "5"
        expect(Cars.cars2a5bFlagDetails()).toEqual("<<");
    });    
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 3 then cars2a5bFlagDetails should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3"
        expect(Cars.cars2a5bFlagDetails()).toEqual("<<");
    });
    it("Given carsWhichFormSelection is 2A, carsWasTheClaimMadeOnOrAfter1October2008 return Yes, and cars2a5aS91TimeLimitsSelection is 2 then cars2a5bFlagDetails should return empty",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2016, 6, 28, 14, 39, 7);
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "2"
        expect(Cars.cars2a5bFlagDetails()).toEqual("");
    });          
    it("Given carsWhichFormSelection return 2A then cars2a6Flagtext should return Yes",function(){
        expect(Cars.cars2a6Flagtext()).toEqual("Yes");
    });    
    it("Given carsWhichFormSelection return 2B then cars2a6Flagtext should return No",function(){
        Cars.carsWhichFormAreYouCompleting = "2B"
        expect(Cars.cars2a6Flagtext()).toEqual("No");
    });
    it("Given carsWhichFormSelection return 2R and cars2rIsQ6InformationCorrect is Yes then cars2a6Flagtext should return No",function(){
        Cars.carsWhichFormAreYouCompleting = "2R"
        expect(Cars.cars2a6Flagtext()).toEqual("No");
    });       
    it("Given carsWhichFormSelection return 2R and cars2rIsQ6InformationCorrect is No then cars2a6Flagtext should return Yes",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2rIsQ6InformationCorrect = "No"
        expect(Cars.cars2a6Flagtext()).toEqual("Yes");
    });
    it("Given cars2a6Flagtext return Yes, then cars2a6FlagAssessmentDetails should return <<",function(){
        expect(Cars.cars2a6FlagAssessmentDetails()).toEqual("<<");
    });
    it("Given cars2a6Flagtext return No, then cars2a6FlagAssessmentDetails should return empty",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2a6FlagAssessmentDetails()).toEqual("");
    });   
    it("Given cars2a6Flagtext return Yes, then cars2a6aFlagYesFillAssessmentDetails should return <<",function(){
        expect(Cars.cars2a6FlagAssessmentDetails()).toEqual("<<");
    });       
    it("Given cars2a6Flagtext return No, then cars2a6aFlagYesFillAssessmentDetails should return empty",function(){
        Cars.carsWhichFormAreYouCompleting = "2R"
        expect(Cars.cars2a6FlagAssessmentDetails()).toEqual("");
    });
    it("Given cars2a6bReasonClaimIsNotReadyForAssessment is 1 then cars2a6bClaimIsNotReadySelection should return 1",function(){
        expect(Cars.cars2a6bClaimIsNotReadySelection()).toEqual("1");
    }); 
    it("Given cars2a6bReasonClaimIsNotReadyForAssessment is 2 then cars2a6bClaimIsNotReadySelection should return 2",function(){
        Cars.cars2a6bReasonClaimIsNotReadyForAssessment = "2";
        expect(Cars.cars2a6bClaimIsNotReadySelection()).toEqual("2");
    });
    it("Given cars2a6bClaimIsNotReadySelection return 1 then cars2a6b1WhenWillClaimBeReadyForAssessment should return 01/12/2016",function(){
        expect(Cars.cars2a6b1WhenWillClaimBeReadyForAssessment()).toEqual("01/12/2016");
    });
    it("Given cars2a6bClaimIsNotReadySelection return 2 then cars2a6b1WhenWillClaimBeReadyForAssessment should return empty",function(){
        Cars.cars2a6bReasonClaimIsNotReadyForAssessment = "2";    
        expect(Cars.cars2a6b1WhenWillClaimBeReadyForAssessment()).toEqual("");
    });     
    it("Given cars2a6Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is Yes then cars2a6b1WhenWillClaimBeReadyForAssessment should return empty",function(){   
        expect(Cars.cars2a6bFlagNoClaimIsNotReady()).toEqual("");
    }); 
    it("Given cars2a6Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is No then cars2a6b1WhenWillClaimBeReadyForAssessment should return <<",function(){   
        Cars.cars2a6bIsClaimReadyForAssessment = "No";
        expect(Cars.cars2a6bFlagNoClaimIsNotReady()).toEqual("<<");
    });        
    it("Given cars2a6Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is No and cars2a6bClaimIsNotReadySelection return 3 then cars2a6bFlagOtherDetails should return <<",function(){   
        Cars.cars2a6bIsClaimReadyForAssessment = "No";
        Cars.cars2a6bReasonClaimIsNotReadyForAssessment = "3";
        expect(Cars.cars2a6bFlagOtherDetails()).toEqual("<<");
    }); 
    it("Given cars2a6Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is No and cars2a6bClaimIsNotReadySelection return 1 then cars2a6bFlagOtherDetails should return empty",function(){   
        Cars.cars2a6bIsClaimReadyForAssessment = "No";
        Cars.cars2a6bReasonClaimIsNotReadyForAssessment = "1";
        expect(Cars.cars2a6bFlagOtherDetails()).toEqual("");
    });             
    it("Given carsWhichFormSelection return 2R and cars2rIsQ7InformationCorrect is No then cars2a7Flagtext should return Yes",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2rIsQ7InformationCorrect = "No";
        expect(Cars.cars2a7Flagtext()).toEqual("Yes");
    });
    it("Given carsWhichFormSelection return 2R and cars2rIsQ7InformationCorrect is Yes then cars2a6Flagtext should return No",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2a7Flagtext()).toEqual("No");
    });    
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is Yes and cars2a7LiabilityIssueSelection return 1 then cars2a7Flag1AllegedContribNeg should return <<",function(){
        expect(Cars.cars2a7Flag1AllegedContribNeg()).toEqual("<<");
    });
    it("Given cars2a7Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is No and cars2a6bClaimIsNotReadySelection return 3 then cars2a6bFlagOtherDetails should return empty",function(){
        Cars.cars2a7IsLiabilityAnIssue = "No";
        expect(Cars.cars2a7Flag1AllegedContribNeg()).toEqual("");
    });
    it("Given cars2a7Flagtext return Yes and cars2a6bIsClaimReadyForAssessment is No and cars2a6bClaimIsNotReadySelection return 2 then cars2a6bFlagOtherDetails should return empty",function(){
        Cars.cars2a7ReasonForLiabilityIssue = "2";
        expect(Cars.cars2a7Flag1AllegedContribNeg()).toEqual("");
    });
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is Yes and cars2a7LiabilityIssueSelection return 1 and cars2a71DoesClaimantAgreeTheyAreAtFault is Yes then cars2a7Flag1YesClaimantAgrees should return <<",function(){
        expect(Cars.cars2a7Flag1YesClaimantAgrees()).toEqual("<<");
    });
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is No and cars2a7LiabilityIssueSelection return 1 and cars2a71DoesClaimantAgreeTheyAreAtFault is Yes then cars2a7Flag1YesClaimantAgrees should return empty",function(){
        Cars.cars2a7IsLiabilityAnIssue = "No"
        expect(Cars.cars2a7Flag1YesClaimantAgrees()).toEqual("");
    });
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is No and cars2a7LiabilityIssueSelection return 2 and cars2a71DoesClaimantAgreeTheyAreAtFault is Yes then cars2a7Flag1YesClaimantAgrees should return empty",function(){
        Cars.cars2a7ReasonForLiabilityIssue = "2"
        expect(Cars.cars2a7Flag1YesClaimantAgrees()).toEqual("");
    });
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is Yes and cars2a7LiabilityIssueSelection return 5 then cars2a7Flag5OtherIssues should return <<",function(){
        Cars.cars2a7ReasonForLiabilityIssue = "5";
        expect(Cars.cars2a7Flag5OtherIssues()).toEqual("<<");
    });
    it("Given cars2a7Flagtext return Yes and cars2a7IsLiabilityAnIssue is Yes then cars2a7FlagYesLiabilityIsAnIssue should return <<",function(){
        expect(Cars.cars2a7FlagYesLiabilityIsAnIssue()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and carsReplyIsDisputeInformationCorrect is Yes then cars2r8DisputeInfoFlagReplytext should return No",function(){
        //Cars.carsWhichFormAreYouCompleting = "2R";
        //Cars.cars2rIsQ7InformationCorrect = "No";
        expect(Cars.cars2r8DisputeInfoFlagReplytext()).toEqual("No");
    });
    it("Given carsWhichFormSelection return 2R and carsReplyIsDisputeInformationCorrect is No then cars2r8DisputeInfoFlagReplytext should return No",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        expect(Cars.cars2r8DisputeInfoFlagReplytext()).toEqual("Yes");
    });
    it("Given carsWhichFormSelection return 2A and cars2r8DisputeInfoFlagReplytext is Yes then cars2a8FlagReplyDisputeInformation should return <<",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        expect(Cars.cars2a8FlagReplyDisputeInformation()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and cars2r8DisputeInfoFlagReplytext is No then cars2a8FlagReplyDisputeInformation should return Empty",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2a8FlagReplyDisputeInformation()).toEqual("");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 1 then cars2a8aFlag1AgreementApplies should return <<",function(){
        //Cars.carsWhichFormAreYouCompleting = "2R";
        //Cars.carsReplyIsDisputeInformationCorrect = "No";
        expect(Cars.cars2a8aFlag1AgreementApplies()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 2 then cars2a8aFlag1AgreementApplies should return empty",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "2";
        expect(Cars.cars2a8aFlag1AgreementApplies()).toEqual("");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 2 then cars2a8aFlagAssessorName should return <<",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "2";
        expect(Cars.cars2a8aFlagAssessorName()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 3 then cars2a8aFlagAssessorName should return <<",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "3";
        expect(Cars.cars2a8aFlagAssessorName()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and cars2a8aSelection is 3 then cars2a8aFlagAssessorName should return empty",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "3";
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2a8aFlagAssessorName()).toEqual("");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 4 then cars2a8aFlagDateMasAppLodged should return <<",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "4";
        expect(Cars.cars2a8aFlagDateMasAppLodged()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 1 then cars2a8aFlagDateMasAppLodged should return empty",function(){
        expect(Cars.cars2a8aFlagDateMasAppLodged()).toEqual("");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 4 then cars2a8aFlagMasMatterNo should return <<",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "4";
        expect(Cars.cars2a8aFlagMasMatterNo()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2A and cars2a8aSelection is 3 then cars2a8aFlagMasMatterNo should return <<",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "3";
        expect(Cars.cars2a8aFlagMasMatterNo()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and cars2a8aSelection is 3 then cars2a8aFlagMasMatterNo should return empty",function(){
        Cars.cars2a8aHowIsCl93Satisfied = "3";
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.cars2a8aFlagMasMatterNo()).toEqual("");
    });
    it("Given carsFlagtextTimeLimitInformation return Yes and carsWasTheClaimMadeOnOrAfter1October2008 returns Yes then cars2aFlagClaimBefore1October2008 should return empty",function(){
        expect(Cars.cars2aFlagClaimBefore1October2008()).toEqual("");
    });
    it("Given carsFlagtextTimeLimitInformation return Yes and carsWasTheClaimMadeOnOrAfter1October2008 returns No then cars2aFlagClaimBefore1October2008 should return <<",function(){
        Cars.carsDateClaimFormSentToInsurer = new Date(2006, 6, 28, 14, 39, 7);
        expect(Cars.cars2aFlagClaimBefore1October2008()).toEqual("<<");
    });
    it("Given carsWasTheClaimMadeOnOrAfter1October2008 return Yes and cars2a5bS91TimeLimitsSelection returns 3 and cars2a5bS912bSelection returns 1 then cars2aS89a3Details should return cars2a5bDetails",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "3";
        expect(Cars.cars2aS89a3Details()).toEqual("cars2a5bDetails");
    });
    it("Given carsWasTheClaimMadeOnOrAfter1October2008 return Yes and cars2a5bS91TimeLimitsSelection returns 5 then cars2aS912dDetails should return cars2a5bDetails",function(){
        Cars.cars2a5bHowIsS91TimeLimitsSatisfied = "5";
        expect(Cars.cars2aS912dDetails()).toEqual("cars2a5bDetails");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return Yes and cars2r8aReplyIsInfoCorrectSelection returns 2 then cars2r8aFlagReplyIncorrectInfo should return <<",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "2"
        expect(Cars.cars2r8aFlagReplyIncorrectInfo()).toEqual("<<");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return Yes and cars2r8aReplyIsInfoCorrectSelection returns 3 then cars2r8aFlagReplyIncorrectInfo should return <<",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "3"
        expect(Cars.cars2r8aFlagReplyIncorrectInfo()).toEqual("<<");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return No and cars2r8aReplyIsInfoCorrectSelection returns 3 then cars2r8aFlagReplyIncorrectInfo should return empty",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "Yes";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "3"
        expect(Cars.cars2r8aFlagReplyIncorrectInfo()).toEqual("");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return Yes and cars2r8aReplyIsInfoCorrectSelection returns 2 then cars2r8aReplyDetailsLabel should return Provide details of how Clause 9.3 is satisifed",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "2"
        expect(Cars.cars2r8aReplyDetailsLabel()).toEqual("Provide details of how Clause 9.3 is satisifed");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return Yes and cars2r8aReplyIsInfoCorrectSelection returns 3 then cars2r8aReplyDetailsLabel should return Provide details why Clause 9.3 is not satisifed",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "3"
        expect(Cars.cars2r8aReplyDetailsLabel()).toEqual("Provide details why Clause 9.3 is not satisifed");
    });
    it("Given cars2r8DisputeInfoFlagReplytext return Yes and cars2r8aReplyIsInfoCorrectSelection returns 1 then cars2r8aReplyDetailsLabel should return not applicable",function(){
        Cars.carsReplyIsDisputeInformationCorrect = "No";
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.cars2r8aReplyIsInfoCorrect = "1"
        expect(Cars.cars2r8aReplyDetailsLabel()).toEqual("not applicable");
    });
    it("Given carsWhichFormSelection return 2A then carsDocumentInformationQuestion should return The list of documents for Q9 must be entered in the document .",function(){
        expect(Cars.carsDocumentInformationQuestion()).toEqual("The list of documents for Q9 must be entered in the document .");
    });
    it("Given carsWhichFormSelection return 2R then carsDocumentInformationQuestion should return The list of documents for Q9 must be entered in the document .",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        expect(Cars.carsDocumentInformationQuestion()).toEqual("The list of documents for Q9 must be entered in the document .");
    });
    it("Given carsWhichFormSelection return 5A then carsDocumentInformationQuestion should return The list of documents for Q7 must be entered in the document .",function(){
        Cars.carsWhichFormAreYouCompleting = "5A";
        expect(Cars.carsDocumentInformationQuestion()).toEqual("The list of documents for Q7 must be entered in the document .");
    });
    it("Given carsWhichFormSelection return 5R then carsDocumentInformationQuestion should return The list of documents for Q7 must be entered in the document .",function(){
        Cars.carsWhichFormAreYouCompleting = "5R";
        expect(Cars.carsDocumentInformationQuestion()).toEqual("The list of documents for Q7 must be entered in the document .");
    });
    it("Given carsWhichFormSelection return 5B then carsDocumentInformationQuestion should return empty",function(){
        Cars.carsWhichFormAreYouCompleting = "5B";
        expect(Cars.carsDocumentInformationQuestion()).toEqual("");
    });
    it("Given carsFlagActForClaimant return Claimant then carsFlagActForClaimant should return <<",function(){
        Client.FileName = "";
        expect(Cars.carsFlagActForClaimant()).toEqual("<<");
    });
    it("Given carsFlagActForClaimant return Insurer then carsFlagActForInsurer should return <<",function(){
        Client.FileName = "Defendant";
        expect(Cars.carsFlagActForInsurer()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2A then carsFlagInsurerInformation should return <<",function(){
        expect(Cars.carsFlagInsurerInformation()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 5A then carsFlagInsurerInformation should return <<",function(){
        Cars.carsWhichFormAreYouCompleting = "5A";
        expect(Cars.carsFlagInsurerInformation()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and carsReplyIsBeingCompleted returns Yes and carsReplyIsInsurerInformationCorrect is No then carsFlagInsurerInformation should return <<",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.carsReplyIsInsurerInformationCorrect = "No";
        expect(Cars.carsFlagInsurerInformation()).toEqual("<<");
    });
    it("Given carsWhichFormSelection return 2R and carsReplyIsBeingCompleted returns Yes and carsReplyIsInsurerInformationCorrect is Yes then carsFlagInsurerInformation should return empty",function(){
        Cars.carsWhichFormAreYouCompleting = "2R";
        Cars.carsReplyIsInsurerInformationCorrect = "Yes";
        expect(Cars.carsFlagInsurerInformation()).toEqual("");
    });
    it("Given carsWhichFormAreYouCompleting is 2A then display should return CARS Form 1A",function(){
        expect(Cars.display()).toEqual("CARS Form 2A");
    });
})