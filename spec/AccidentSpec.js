/**
 * Created by George.Zheng on 2/08/2016.
 */
describe("Accident Details Calculation", function(){
    beforeEach(function() {
        Accident.fullAddressOfDriverIfNotClient = "Accident address";
        Accident.fullAddressOfOwnerIfNotClient = "Owner Accident address";
        Accident.dateOfAccidentIncident = new Date(2016, 01, 01, 0, 0, 0);
        Accident.dateOfAccidentMonthDateUnknown = "January";
        Accident.dateOfAccidentYearDateUnknown = 2016;
        Accident.periodFrom = new Date(2016, 01, 01, 0, 0, 0);
        Accident.periodTo = new Date(2016, 03, 01, 0, 0, 0);
        Accident.statuteOfLimitationsOverride = new Date(2016, 01, 01, 0, 0, 0);
        Accident.fullNameOfDriverIfNotClient = "George";
        Accident.fullNameOfOwnerIfNotClient = "George";
        Accident.plaintiffWasDriverPassangerEtc = "driver";
        Accident.numberOfVehiclesInvolvedInAccident = 1;
        Archive.archiveNumber = 1;
        Archive.dateArchived = new Date(2016,01,01,0,0,0);
        Archive.archivedBy = "George";
    });
    it("Given fullAddressOfDriverIfNotClient is Accident address should return Accident address",function(){
        expect(Accident.addressOfDriverOfPlaintiffsVehicle()).toEqual("Accident address");
    });
    it("Given fullAddressOfDriverIfNotClient is not defiend should return streetAddressAcrMult",function(){
        spyOn(matter.client, "streetAddressAcrMult").and.returnValue("streetAddressAcrMult");
        Accident.fullAddressOfDriverIfNotClient = "";
        expect(Accident.addressOfDriverOfPlaintiffsVehicle()).toEqual("streetAddressAcrMult");
    });
    it("Given dateOfAccidentIncident is 01/02/2016 and dateOfAccidentCalc should return streetAddressAcrMult",function(){
        expect(Accident.dateOfAccidentCalc()).toEqual("Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)");
    });
    it("Given dateOfAccidentIncident is not defiend, dateOfAccidentMonthDateUnknown is 1 and dateOfAccidentYearDateUnknown is 2016" +
        " dateOfAccidentCalc should return January 2016",function(){
        Accident.dateOfAccidentIncident = "";
        expect(Accident.dateOfAccidentCalc()).toEqual("January 2016");
    });
    it("Given dateOfAccidentIncident is 01/02/2016, dateOfAccidentMonthDateUnknown is 1 and dateOfAccidentYearDateUnknown is 2016" +
        " layoutTitle should return Accident/Incident Date: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)   Statute of Limitations: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)",function(){
        expect(Accident.layoutTitle()).toEqual("Accident/Incident Date: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)   Statute of Limitations: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)");
    });
    it("Given dateOfAccidentMonthDateUnknown is not defiend, dateOfAccidentMonthDateUnknown is 1 and dateOfAccidentYearDateUnknown is 2016" +
        " layoutTitle should return Accident/Incident Date: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)   Statute of Limitations: Mon Feb 01 2016 00:00:00 GMT+1100 (AUS Eastern Daylight Time)",function(){
        Accident.dateOfAccidentIncident = null;
        expect(Accident.layoutTitle()).toEqual("Accident/Incident Date: January 2016");
    });
    it("Given fullNameOfDriverIfNotClient is George" +
        " nameOfDriverOfPlaintiffsVehicle should return George",function(){
        expect(Accident.nameOfDriverOfPlaintiffsVehicle()).toEqual("George");
    });
    it("Given fullNameOfDriverIfNotClient is not defined" +
        " nameOfDriverOfPlaintiffsVehicle should return GeorgeFullLegalDescription",function(){
        Accident.fullNameOfDriverIfNotClient = "";
        spyOn(matter.client, "fullLegalDescriptMultiple").and.returnValue("GeorgeFullLegalDescription");
        expect(Accident.nameOfDriverOfPlaintiffsVehicle()).toEqual("GeorgeFullLegalDescription");
    });
    it("Given plaintiffWasDriverPassangerEtc is driver" +
        " plaintiffWas should return driving",function(){
        expect(Accident.plaintiffWas()).toEqual("driving");
    });
    it("Given plaintiffWasDriverPassangerEtc is passenger" +
        " plaintiffWas should return a passenger in",function(){
        Accident.plaintiffWasDriverPassangerEtc = "passenger";
        expect(Accident.plaintiffWas()).toEqual("a passenger in");
    });
    it("Given dateOfAccidentIncident is 01/02/2016" +
        " statOfLimitAccInjDate should return  Mon Jan 31 2028 00:00:00 GMT+1100 (AUS Eastern Daylight Time)",function(){
        expect(Accident.statOfLimitAccInjDate()).toEqual("Mon Jan 31 2028 00:00:00 GMT+1100 (AUS Eastern Daylight Time)");
    });
    it("Given dateOfAccidentIncident is 01/02/2016" +
        " statOfLimitAccInjDate should return  Mon Jan 31 2028 00:00:00 GMT+1100 (AUS Eastern Daylight Time)",function(){
        expect(Archive.layoutDisplay()).toEqual("No: 1 Date: 01/02/16 By: George");
    });
});
