/**
 * Created by George.Zheng on 4/08/2016.
 */
describe("Accident Details Calculation", function(){
    beforeEach(function() {
        Asset.assetType = "Cash";
        Asset.description = "Cash Description";
        Asset.deceasedsShareField = 10;
        Asset.ifTenantsInCommonHowManyShares = 10;
        Asset.saRetentionNoOfMonths = 10;
        Asset.saRetentionAmount = 100;
        Asset.saAmountOriginalBond = 2000;
        Asset.saPortionOfBondUnpaid = 10;
        Asset.saAccomBondInterest = 10;
        Asset.saAccomBondPrepaidMaintenance = 100;
        Asset.saAccomBondPersonalAccount = 10;
        Asset.valueAtDeath = 100;
        Asset.interestAccrued = 100;
        matter.state = "VIC";
    });
    it("Given assetType is Cash and description is Chas Description ainvGeneralCellDesc should return" +
        "Cash /n Cash Description",function(){
        expect(Asset.ainvGeneralCellDesc()).toEqual("Cash" + String.fromCharCode(11) + "Cash Description");
    });
    it("Given saRetentionNoOfMonths is 10 and saRetentionAmount is 100 saRetentionTotalAmount should return" +
        "$1000.00",function(){
        expect(Asset.saRetentionTotalAmount()).toEqual("$1,000.00");
    });
    it("Given saAmountOriginalBond is 2000 and saPortionOfBondUnpaid is 10 saRetentionTotalAmount" +
        "saAccomBondInterest is 10 and saRetentionTotalAmount() return 1000" +
        "saAccomBondTotalRepaid should return $980.00",function(){
        expect(Asset.saAccomBondTotalRepaid()).toEqual("$980.00");
    });
    it("Given saAccomBondPrepaidMaintenance is 100 and saAccomBondPersonalAccount is 10" +
        "saAccomBondTotalRepaid() return 980" +
        "saAccomBondTotalRefund should return $1090.00",function(){
        expect(Asset.saAccomBondTotalRefund()).toEqual("$1,090.00");
    });
    it("Given valueAtDeath is 100 and interestAccrued is 100 totalValuePlusInterest should return" +
        " $200.00",function(){
        expect(Asset.totalValuePlusInterest()).toEqual("$200.00");
    });
    it("Given matter state is SA and assetType includes Accommodation Bond" +
        "saAccomBondTotalRefund() return 1090" +
        "deceasedsShareCalc should return $1,090.00",function(){
        matter.state = "SA";
        Asset.assetType = "Accommodation Bond";
        expect(Asset.deceasedsShareCalc()).toEqual("$1,090.00");
    });
});
