/**
 * Created by George.Zheng on 27/07/2016.
 */
describe("Lease Details Forms Calculation", function(){
    beforeEach(function() {
        Lease.contributionAmount = 100;
        Lease.contributionOfBaseRent = 10;
        matter.rent.initialRentPa = 1000;
        Lease.contributionAdditionalRentYesNo = "Yes";
        Lease.titleReferenceOfProperty = "propertyReferenceTitle";
        Lease.propertyBuildingName = "GeorgeBuilding",
        Lease.flatUnitShop = "Unit 1";
        Lease.streetNo = "1";
        Lease.street = "George St";
        Lease.suburb = "Docklands";
        Lease.termOfLease = "1 year";
        Lease.duplicateDocuments = 0;
        Lease.leaseCommencementDate = new Date(2016, 01, 01, 0, 0, 0);
        Lease.leaseTerminationDate = new Date(2016, 03, 01, 0, 0, 0);
    });
    it("Given contributionAmount is 100 should return $100",function(){
        expect(Lease.contributionPromLevyBaseAmt()).toEqual("$100.00");
    });
    it("Given contributionAmount is 0 and contirbutionOfBaseRent " +
        "and matter.rent.initialRentPa is 1000 is 10 should return $100.00",function(){
        Lease.contributionAmount = 0;
        expect(Lease.contributionPromLevyBaseAmt()).toEqual("$100.00");
    });
    it("Given contributionAmount is 0 and contirbutionOfBaseRent is 0" +
        "and matter.rent.initialRentPa is 0 should return $0.00",function(){
        Lease.contributionAmount = 0;
        Lease.contributionOfBaseRent = 0;
        matter.rent.initialRentPa = 0;
        expect(Lease.contributionPromLevyBaseAmt()).toEqual("$0.00");
    });
    it("Given contributionAdditionalRentYesNo is yes contributionPromSdLevyBaseAmt should return $100",function(){
        expect(Lease.contributionPromSdLevyBaseAmt()).toEqual("$100.00");
    });
    it("Given contributionAdditionalRentYesNo is No contributionPromSdLevyBaseAmt should return $0.00",function(){
        Lease.contributionAdditionalRentYesNo = "No";
        expect(Lease.contributionPromSdLevyBaseAmt()).toEqual("$0.00");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 termYears should return 2",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        expect(Lease.termYears()).toEqual(2);
    });
    it("Given leaseCommencementDate is 01/12/1999 and leaseTerminationDate is 01/01/2000 termYears should return 0",function(){
        Lease.leaseCommencementDate = new Date(1999,11,00);
        Lease.leaseTerminationDate = new Date(2000,0,00);
        expect(Lease.termYears()).toEqual(0);
    });
    it("Given leaseCommencementDate is 01/12/1999 and leaseTerminationDate is 02/12/1999 termDays should return 1",function(){
        Lease.leaseCommencementDate = new Date(1999,11,00);
        Lease.leaseTerminationDate = new Date(1999,11,01);
        expect(Lease.termDays()).toEqual(2);
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 contributionPromotionLevyCalc should return 400.164",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        expect(Lease.contributionPromotionLevyCalc()).toEqual("$200.27");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 and the contributionAmount is 0 and contributionOfBaseRent is 10" +
        " contributionPromotionLevyCalc and initialRentPa is 1000 should return 400.164",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        Lease.contributionAmount = 0;
        expect(Lease.contributionPromotionLevyCalc()).toEqual("$200.27");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 and the contributionAmount is 0 and contributionOfBaseRent is 20" +
        " contributionPromotionLevyCalc and initialRentPa is 1000 should return $800.27",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        Lease.contributionAmount = 0;
        Lease.contributionOfBaseRent = 20;
        expect(Lease.contributionPromotionLevyCalc()).toEqual("$400.55");
    });
    it("Given propertyBuildingName is GeorgeBuilding, flatUnitShop is Unit 1, streetNo is 1, street George St and suburb is Docklands" +
        "propertyDescriptionAcross should return Unit 1, GeorgeBuilding, 1 George St, Docklands",function(){
        expect(Lease.propertyDescriptionAcross()).toEqual("Unit 1, GeorgeBuilding, 1 George St, Docklands");
    });
    it("Given propertyBuildingName is not defined, flatUnitShop is Unit 1, streetNo is 1, street George St and suburb is Docklands" +
        "propertyDescriptionAcross should return Unit 1, GeorgeBuilding, 1 George St, Docklands",function(){
        Lease.propertyBuildingName = "";
        expect(Lease.propertyDescriptionAcross()).toEqual("Unit 1/1 George St, Docklands");
    });
    it("Given propertyDescriptionAcross returns Unit 1/1 George St, Docklands" +
        "premisesDetailsCalc should return TITLE REFERENCE OF PROPERTY: propertyReferenceTitle" +
        "DESCRIPTION OF PROPERTY: Unit 1, GeorgeBuilding, 1 George St, Docklands",function(){
        expect(Lease.premisesDetailsCalc()).toEqual("TITLE REFERENCE OF PROPERTY: propertyReferenceTitle"+ "\n" +"DESCRIPTION OF PROPERTY: Unit 1, GeorgeBuilding, 1 George St, Docklands");
    });
    it("Given matter.property.titleReference returns Title reference" +
        "premisesDetailsCalc should return TITLE REFERENCE OF PROPERTY: propertyReferenceTitle" +
        "DESCRIPTION OF PROPERTY: Unit 1, GeorgeBuilding, 1 George St, Docklands",function(){
        expect(Lease.reportPremisesDetails()).toEqual("propertyAddressAcrossString");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 and term of lease 1 year" +
        "termOfLeaseDisplay should return 1 year",function() {
        expect(Lease.termOfLeaseDisplay()).toEqual("1 year");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 and term of lease 1 year" +
        "termOfLeaseDisplay should return two (2) Year and one (1) day",function() {
        Lease.termOfLease = "";
        Lease.leaseCommencementDate = new Date(2000, 0, 00);
        Lease.leaseTerminationDate = new Date(2002, 0, 00);
        expect(Lease.termOfLeaseDisplay()).toEqual("two (2) Years and one (1) Day");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/02/2002 and term of lease 1 year" +
        "termOfLeaseDisplay should return two (2) Year, one (1) month and one (1) day",function() {
        Lease.termOfLease = "";
        Lease.leaseCommencementDate = new Date(2000,00,02);
        Lease.leaseTerminationDate = new Date(2002,01,2);
        expect(Lease.termOfLeaseDisplay()).toEqual("two (2) Years, one (1) Month and one (1) Day");
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/02/2002 and term of lease 1 year" +
        "termOfLeaseDisplay should return two (2) Year, one (1) month and one (1) day",function() {
        Lease.termOfLease = "";
        Lease.leaseCommencementDate = new Date(2000,00,02);
        Lease.leaseTerminationDate = new Date(2002,01,2);
        expect(Lease.termOfLeaseDisplay()).toEqual("two (2) Years, one (1) Month and one (1) Day");
    });
    it("Given leaseCommencementDate is 31/12/1999 and leaseTerminationDate is 31/12/2001 termMonths should return 0",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        expect(Lease.termMonths()).toEqual(0);
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 31/01/2002 termMonths should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,00,0);
        Lease.leaseTerminationDate = new Date(2002,01,0);
        expect(Lease.termMonths()).toEqual(1);
    });
    it("Given leaseCommencementDate is 01/03/2000 and leaseTerminationDate is 30/01/2002 termMonths should return 11",function(){
        Lease.leaseCommencementDate = new Date(2000,1,30);
        Lease.leaseTerminationDate = new Date(2002,01,0);
        expect(Lease.termMonths()).toEqual(10);
    });
    it("Given leaseCommencementDate is 01/03/2000 and leaseTerminationDate is 30/01/2001 termMonths should return 11",function(){
        Lease.leaseCommencementDate = new Date(2000,1,30);
        Lease.leaseTerminationDate = new Date(2001,01,0);
        expect(Lease.termMonths()).toEqual(10);
    });
    it("Given leaseCommencementDate is 30/12/2000 and leaseTerminationDate is 29/01/2001 termMonths should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,11,30);
        Lease.leaseTerminationDate = new Date(2001,00,29);
        expect(Lease.termMonths()).toEqual(1);
    });
    it("Given leaseCommencementDate is 30/12/2000 and leaseTerminationDate is 30/01/2001 termMonths should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,11,30);
        Lease.leaseTerminationDate = new Date(2001,00,30);
        expect(Lease.termMonths()).toEqual(1);
    });
    it("Given leaseCommencementDate is 30/12/2000 and leaseTerminationDate is 28/01/2001 termMonths should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,11,30);
        Lease.leaseTerminationDate = new Date(2001,00,28);
        expect(Lease.termMonths()).toEqual(0);
    });
    it("Given leaseCommencementDate is 30/12/2000 and leaseTerminationDate is 29/01/2002 termMonths should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,11,30);
        Lease.leaseTerminationDate = new Date(2002,00,29);
        expect(Lease.termMonths()).toEqual(1);
    });
    it("Given leaseCommencementDate is 01/01/2000 and leaseTerminationDate is 01/01/2002 termDays should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,0,00);
        Lease.leaseTerminationDate = new Date(2002,0,00);
        expect(Lease.termDays()).toEqual(1);
    });
    it("Given leaseCommencementDate is 28/02/2000 and leaseTerminationDate is 01/01/2002 termDays should return 1",function(){
        Lease.leaseCommencementDate = new Date(2000,02,0);
        Lease.leaseTerminationDate = new Date(2002,01,0);
        expect(Lease.termDays()).toEqual(3);
    });
    it("Given leaseCommencementDate is 28/02/2000 and leaseTerminationDate is 15/01/2002 termDays should return 17",function(){
        Lease.leaseCommencementDate = new Date(2000,02,0);
        Lease.leaseTerminationDate = new Date(2002,0,16);
        expect(Lease.termDays()).toEqual(17);
    });
    it("Given leaseCommencementDate is 28/02/2000 and leaseTerminationDate is 17/01/2002 termDays should return 19",function(){
        Lease.leaseCommencementDate = new Date(2000,02,0);
        Lease.leaseTerminationDate = new Date(2002,0,18);
        expect(Lease.termDays()).toEqual(19);
    });
    it("Given leaseCommencementDate is 31/03/2000 and leaseTerminationDate is 10/01/2002 termDays should return 11",function(){
        ////TODO - change the expectation once the termMonths is there
        Lease.leaseCommencementDate = new Date(2000,03,01);
        Lease.leaseTerminationDate = new Date(2002,0,11);
        expect(Lease.termDays()).toEqual(11);
    });
    it("Given leaseCommencementDate is 31/03/2000 and leaseTerminationDate is 15/01/2002 termDays should return 15",function(){
        ////TODO - change the expectation once the termMonths is there
        Lease.leaseCommencementDate = new Date(2000,03,01);
        Lease.leaseTerminationDate = new Date(2002,0,16);
        expect(Lease.termDays()).toEqual(16);
    });
    it("Given leaseCommencementDate is 31/12/1999 and leaseTerminationDate is 01/01/2000 termDays should return 2",function(){
        Lease.leaseCommencementDate = new Date(1999,11,31);
        Lease.leaseTerminationDate = new Date(2000,00,01);
        expect(Lease.termDays()).toEqual(2);
    });
    it("Given contributionAmount is 100 leaseCommencementDate is 01/02/2016 and leaseTerminationDate is 01/04/2016 termDays should return 2",function(){
        expect(Lease.totalContributionAmountFullTerm()).toEqual("$16.94");
    });
    it("Given matter.rent.totalLeaseStampDuty is 0 and totalDuplicateDocumentFees should return 0",function(){
        expect(Lease.totalDuplicateDocumentFees()).toEqual(0);
    });
    it("Given matter.rent.totalLeaseStampDuty is 2 and duplicateDocuments is 1 totalDuplicateDocumentFees should return 2",function(){
        spyOn(matter.rent, "totalLeaseStampDuty").and.returnValue(2);
        expect(Lease.totalDuplicateDocumentFees()).toEqual(2);
    });
    it("Given matter.rent.totalLeaseStampDuty is 2 and duplicateDocuments is 0 totalDuplicateDocumentFees should return 2",function(){
        spyOn(matter.rent, "totalLeaseStampDuty").and.returnValue(2);
        Lease.duplicateDocuments = 0;
        expect(Lease.totalDuplicateDocumentFees()).toEqual(2);
    });
});
