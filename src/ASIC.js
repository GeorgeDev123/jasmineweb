/**
 * Created by George.Zheng on 4/08/2016.
 */
var Asic = {
    
}


var Archive = {
    archiveNumber: "1",
    dateArchived: new Date(2016,01,01,0,0,0),
    archivedBy: "George",
    layoutDisplay: function () {
        // ArchiveDetails.layoutDisplay. JS Calculation created on 3/08/2016 by GZ
        var result = '';
        result = "No: " + this.archiveNumber + " Date: ";
        if (LEAP.utilities.date.isValid(this.dateArchived)) {
            result += this.dateArchived.toString("dd/MM/yy");
        }
        result += " By: " + this.archivedBy;
        return result;
    }
}