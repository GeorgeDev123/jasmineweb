/**
 * Created by George.Zheng on 2/08/2016.
 */
var Accident ={
    fullAddressOfDriverIfNotClient: "Accident address",
    fullAddressOfOwnerIfNotClient: "Owner Accident address",
    dateOfAccidentIncident: new Date(2016, 01, 01, 0, 0, 0),
    dateOfAccidentMonthDateUnknown: "January",
    dateOfAccidentYearDateUnknown:2016,
    periodFrom: new Date(2016, 01, 01, 0, 0, 0),
    periodTo: new Date(2016, 03, 01, 0, 0, 0),
    statuteOfLimitationsOverride: new Date(2016, 01, 01, 0, 0, 0),
    fullNameOfDriverIfNotClient: "George",
    fullNameOfOwnerIfNotClient: "George",
    plaintiffWasDriverPassangerEtc: "driver",
    numberOfVehiclesInvolvedInAccident: 1,
    /***
     * If
     (
        [Full address of driver - if not client]#""
        ~[Full address of driver - if not client]
        ~[{Client}Street Address (Acr) - Mult]
     )
     * @returns {string}
     */
    addressOfDriverOfPlaintiffsVehicle: function () {
        var result = "";
        if(this.fullAddressOfDriverIfNotClient){
            result = this.fullAddressOfDriverIfNotClient;
        }
        else{
            result = matter.client.streetAddressAcrMult();
        }
        return result;
    },
    /**
     * If
     (
        [Full address of owner - if not client]#""
        ~[Full address of owner - if not client]
        ~[{Client}Street Address (Acr) - Mult]
     )
     */
    addressOfOwnerOfPlaintiffsVehicle: function () {
        var result = 0;
        if(this.fullAddressOfOwnerIfNotClient){
            result = this.fullAddressOfOwnerIfNotClient;
        }
        else{
            result = matter.client.streetAddressAcrMult();
        }
        return result;
    },
    /***
     * First_true
     (
         [Date of accident/incident]#!00/00/00!
         ~Format
         (
         [Date of accident/incident]
         ~"Long"
         )

     ~And
     (
         [Date of accident month - date unknown]#""
         ~[Date of accident year - date unknown]#""
     )
     ~[Date of accident month - date unknown]+" "+[Date of accident year - date unknown]

     ~[Date of accident year - date unknown]#""
     ~[Date of accident year - date unknown]

     ~[Period from]>!00/00/00!
     ~[Period from]+" to "+[Period to]

     ~1=1
     ~""
     )
     */
    dateOfAccidentCalc: function () {
        var result = "";
        if(LEAP.utilities.date.isValid(this.dateOfAccidentIncident)){
            result = this.dateOfAccidentIncident.toString("dd MMMM, yyyy");
        }
        else if(this.dateOfAccidentMonthDateUnknown && this.dateOfAccidentYearDateUnknown){
            result = this.dateOfAccidentMonthDateUnknown + " " + this.dateOfAccidentYearDateUnknown;
        }
        else if(this.dateOfAccidentYearDateUnknown){
            result = this.dateOfAccidentYearDateUnknown;
        }
        else if(LEAP.utilities.date.isValid(this.periodFrom)){
            result = this.periodFrom.toString("dd MMMM, yyyy") + " to ";
                if(LEAP.utilities.date.isValid(this.periodTo)){
                    result += this.periodTo.toString("dd MMMM, yyyy");
                }
        }
        return result;
    },
    /**
     * First_True
     (
     [{Defendant 2}[Card] Short Name]=""
     ~[{Defendant}Full Legal Descript. (Multiple)]+"
     "+"of "+[{Defendant}Street Address (Acr) - Mult]+"
     "+ASCII_Character
     (
     9
     )+"Defendant"
     ~1=1
     ~[{Defendant}Full Descript (sing) without ACN]+"
     "+"of "+[{Defendant}Street Address (Acr) - Sing]+"
     "+ASCII_Character
     (
     9
     )+"First Defendant"+"

     AND
     "+[{Defendant 2}Full Descript (sing) without ACN]+"
     "+"of "+[{Defendant 2}Street Address (Acr) - Sing]+"
     "+ASCII_Character
     (
     9
     )+" Second Defendant"
     )
     */
    firstSecondDefendant: function () {
        // AccidentIncidentDetails.firstSecondDefendant. JS Calculation created on 2/08/2016 by GZ
        /**
         * Require fullLegalDescriptMultiple(), streetAddressAcrMult(), fullDescriptSingWithoutAcn, streetAddressAcrSing functions to be implemented before fully testing
         *
         **/
        var result = '';
        if (!matter.defendantList) {
            return result;
        } else if (matter.defendantList && matter.defendantList.length === 1) {
            result = matter.defendant.fullLegalDescriptMultiple() + "of " + matter.defendant.streetAddressAcrMult() + String.fromCharCode(9) + "Defendant";
        } else {
            result += matter.defendantList[0].fullDescriptSingWithoutAcn() + "of " +
                matter.defendantList[0].streetAddressAcrSing() + String.fromCharCode(9) + "First Defendant" + "\r";
            result += matter.defendantList[1].fullDescriptSingWithoutAcn() + "of " + matter.defendantList[1].streetAddressAcrSing() + String.fromCharCode(9) + "Second Defendant" + "\r";
        }
        return result;
    },
    /**
     * "Accident/Incident Date: "+First_True
     (
     [Date of accident/incident]#!00/00/00!
     ~[Date of accident/incident]+"     Statute of Limitations: "+If
     (
     [Statute of limitations override]>!00/00/00!
     ~[Statute of limitations override]
     ~[Statute of Limitations]
     )


     ~[Date of accident month - date unknown]#""
     ~[Date of accident month - date unknown]+" "+[Date of accident year - date unknown]


     ~1=1
     ~""
     )
     */
    layoutTitle: function () {
        var result = "";
        result = "Accident/Incident Date: ";
        if(LEAP.utilities.date.isValid(this.dateOfAccidentIncident)){
            result += this.dateOfAccidentIncident.toString("dd MMMM, yyyy") +
            "   Statute of Limitations: ";
            if(LEAP.utilities.date.isValid(this.statuteOfLimitationsOverride)){
                result += this.statuteOfLimitationsOverride.toString("dd MMMM, yyyy");
            }
            else{
                var tempStatue = this.statuteOfLimitations();
                if(LEAP.utilities.date.isValid(tempStatue)) {
                    result += tempStatue.toString("dd MMMM, yyyy");
                }
            }
        }
        else if(this.dateOfAccidentMonthDateUnknown){
            result += this.dateOfAccidentMonthDateUnknown + " " + this.dateOfAccidentYearDateUnknown;
        }
        return result;
    },
    /**
     * If
     (
        [Full name of driver - if not client]#""
        ~[Full name of driver - if not client]
        ~[{Client}Full Legal Descript. (Multiple)]
     )
     */
    nameOfDriverOfPlaintiffsVehicle: function () {
        var result= "";
        if(this.fullNameOfDriverIfNotClient){
            result += this.fullNameOfDriverIfNotClient;
        }
        else {
            result += matter.client.fullLegalDescriptMultiple();
        }
        return result;
    },
    /**
     * If
     (
         [Full name of owner - if not client]#""
         ~[Full name of owner - if not client]
         ~[{Client}Full Legal Descript. (Multiple)]
     )
     */
    nameOfOwnerOfPlaintiffsVehicle: function () {
        var result= "";
        if(this.fullNameOfOwnerIfNotClient){
            result += this.fullNameOfOwnerIfNotClient;
        }
        else {
            result += matter.client.fullLegalDescriptMultiple();
        }
        return result;
    },
    /**
     * First_True
     (
     [Plaintiff was - driver, passanger etc]="driver"
     ~"driving"
     ~[Plaintiff was - driver, passanger etc]="passenger"
     ~"a passenger in"
     ~[Plaintiff was - driver, passanger etc]="rider"
     ~"a rider on"
     )
     */
    plaintiffWas:function () {
        var result = "";
        if(this.plaintiffWasDriverPassangerEtc === "driver"){
            result = "driving";
        }
        else if (this.plaintiffWasDriverPassangerEtc === "passenger"){
            result = "a passenger in";
        }
        else if(this.plaintiffWasDriverPassangerEtc === "rider"){
            result = "a rider on";
        }
        return result;
    },
    //(Add_Years_to_Date([Date of accident/incident]~12)-1)
    statOfLimitAccInjDate: function () {
        var result = "";
        if(LEAP.utilities.date.isValid(this.dateOfAccidentIncident)){
            var tempDate = new Date(this.dateOfAccidentIncident.getTime());
            tempDate.setYear(tempDate.getFullYear() + 12);
            tempDate.setMonth(tempDate.getMonth() - 1);
            var day = LEAP.utilities.date.daysInMonth(tempDate.getFullYear(), tempDate.getMonth());
            tempDate.setDate(day);
            result = tempDate;
        }
        return result.toString("dd MMMM, yyyy");
    },
    /**
     * First_True
     (
     And
     (
     [{Matter}State]="VIC"
     ~Text_Position
     (
     [{Matter}Matter Type]
     ~"MVA"
     )=0
     ~Years_Between
     (
     If
     (
     [{Card}Court Plaintiff is Client Other Side or Plaintiff]="Client"
     ~[{Client}[People] Date of Birth]
     ~[{Other Side}[People] Date of Birth]
     )
     ~[Date of accident/incident]
     )<21
     )
     ~
     (
     Add_Years_to_Date
     (
     If
     (
     [{Card}Court Plaintiff is Client Other Side or Plaintiff]="Client"
     ~[{Client}[People] Date of Birth]
     ~[{Other Side}[People] Date of Birth]
     )
     ~24
     )-1
     )


     ~And
     (
     [{Matter}State]#"NSW"
     ~Years_Between
     (
     If
     (
     [{Card}Court Plaintiff is Client Other Side or Plaintiff]="Client"
     ~[{Client}[People] Date of Birth]
     ~[{Other Side}[People] Date of Birth]
     )
     ~[Date of accident/incident]
     )<18
     )
     ~
     (
     Add_Years_to_Date
     (
     If
     (
     [{Card}Court Plaintiff is Client Other Side or Plaintiff]="Client"
     ~[{Client}[People] Date of Birth]
     ~[{Other Side}[People] Date of Birth]
     )
     ~21
     )-1
     )


     ~1=1
     ~
     (
     Add_Years_to_Date
     (
     First_True
     (
     [Date cause of action is discoverable]>!00/00/00!
     ~[Date cause of action is discoverable]
     ~1=1
     ~[Date of accident/incident]
     )
     ~3
     )-1
     )
     )
     */
    statOfLimitDiscoverableDate:function () {
        // if()
        // if(matter.state === "VIC" && matter.matterType.toUpperCase().includes("MVC")
        // && LEAP.utilities.date.yearsBetween()){
        // }
    },
    statuteOfLimitations: function () {
    },
    /**
     * First_True
     (
     Multiple_Instance
     (
         [{Defendant}[Card] Short Name]
         ~2
         ~*
     )#""
        ~"4"

     ~And
     (
        [Plaintiff was - driver, passanger etc]="passenger"
        ~[Number of vehicles involved in accident]=1
     )
        ~"1"

     ~And
     (
        [Plaintiff was - driver, passanger etc]="pedestrian"
        ~[Number of vehicles involved in accident]=1
     )
        ~"2"

     ~And
     (
     Or
     (
         [Plaintiff was - driver, passanger etc]="passenger"
         ~[Plaintiff was - driver, passanger etc]="driver"
     )
          ~[Number of vehicles involved in accident]=2
     )
     ~"3"

     ~1=1
         ~"5"
     )
     */
    supremeCourtStatementOfClaim: function () {
        var result = 0;
        if (matter.defendantList && matter.defendantList.length > 1) {
            result = 4;
        } else if (this.plaintiffWasDriverPassangerEtc === "passenger" &&
            this.numberOfVehiclesInvolvedInAccident === 1) {
            result = 1;
        } else if (this.plaintiffWasDriverPassangerEtc === "pedestrian" &&
            this.numberOfVehiclesInvolvedInAccident === 1) {
            result = 2;
        } else if ((this.plaintiffWasDriverPassangerEtc === "passenger" || this.plaintiffWasDriverPassangerEtc === "driver") &&
            this.numberOfVehiclesInvolvedInAccident === 2) {
            result = 3;
        } else {
            result = 5;
        }

        return result;
    }
}