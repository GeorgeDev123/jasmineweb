var Cars = {
    carsWhichFormAreYouCompleting: "2A",
    cars2rIsQ5TimeLimitInformationCorrect: "No",
    carsDateClaimFormReceivedByInsurer: new Date(1993, 6, 28, 14, 39, 7),
    carsDateClaimFormSentToInsurer: new Date(2012, 6, 28, 14, 39, 7),
    cars2a5aHowIsS91TimeLimitsSatisfied: "1",
    cars2a5aHowIsS912Satisfied: "1",
    cars2a5bHowIsS91TimeLimitsSatisfied: "1",
    cars2a5bHowIsS912bSatisfied: "1",
    cars2a5bHowIsS912aSatisfied: "1",
    cars2rIsQ6InformationCorrect: "Yes",
    cars2a6aIsClaimNotSuitableForAssessment: "Yes",
    cars2a6bReasonClaimIsNotReadyForAssessment: "1",
    cars2a6bWhenWillClaimBeReadyForAssessment: "01/12/2016",
    cars2a6bIsClaimReadyForAssessment: "Yes",
    cars2rIsQ7InformationCorrect: "Yes",
    cars2a7ReasonForLiabilityIssue: "1",
    cars2a7IsLiabilityAnIssue: "Yes",
    cars2a71DoesClaimantAgreeTheyAreAtFault: "Yes",
    carsReplyIsDisputeInformationCorrect: "Yes",
    cars2a8aWhatIsAgreedForNoneconomicLoss: "1",
    cars2a8aHowIsCl93Satisfied: "1",
    cars2a5bDetails: "cars2a5bDetails",
    cars2r8aReplyIsInfoCorrect: "1",
    cars5aQ6aDisputeInformation: "A",
    cars5aQ6bDisputeInformation: "A",
    carsWhoIsTheClaimMadeAgainst: "1",
    carsReplyIsInsurerInformationCorrect: "Yes",
    /**
     * If
     (
     And
     (s
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="No"
     ~[CARS 2A 5A s91 Time Limits selection]="1"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5aFlag1S911a: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "No" && this.cars2a5aS91TimeLimitsSelection() === "1") {
            result = this.carsFlagText();
        }
        return result;
    },

    /**
     If
     (
     Or
     (
     [CARS Which form Selection]="2A"
     ~[CARS Which form Selection]="5A"
     ~And
     (
     [CARS Which form Selection]="2R"
     ~[CARS 2R is Q5 Time Limit Information Correct]#"Yes"
     )
     )
     ~"Yes"
     ~"No"
     )
     */
    carsFlagtextTimeLimitInformation: function () {
        var result = "";
        if ((this.carsWhichFormSelection() === "2A" || this.carsWhichFormSelection() === "5A") ||
            (this.carsWhichFormSelection() === "2R" && this.cars2rIsQ5TimeLimitInformationCorrect !== "Yes")) {
            result = "Yes";
        }
        else {
            result = "No";
        }
        return result;
    },
    /**
     * Text_Extract([CARS Which form are you completing]~1~2)
     */
    carsWhichFormSelection: function () {
        var result = "";
        result = this.carsWhichFormAreYouCompleting.substring(0, 2);
        return result;
    },
    /**
     * First_True
     (
     [CARS Acting for Claimant or Insurer]="Insurer"
     ~If
     (
        [CARS Date Claim Form Received by Insurer]>!30/9/08!
        ~"Yes"
        ~"No"
     )


     ~1=1
     ~If
     (
     [CARS Date Claim Form Sent to Insurer]>!30/9/08!
        ~"Yes"
        ~"No"
     )
     )
     */
    carsWasTheClaimMadeOnOrAfter1October2008: function () {
        var result = "";
        var criticalDate = new Date(2008, 09, 30, 0, 0, 0);
        if (this.carsActingForClaimantOrInsurer() === "Insurer") {
            if(this.carsDateClaimFormReceivedByInsurer > criticalDate) {
                result = "Yes";
            }
            else {
                result = "No";
            }
        }
        else {
            if(this.carsDateClaimFormSentToInsurer > criticalDate) {
                result = "Yes";
            }
            else {
                result = "No";
            }
        }
        return result;
    },
    /**
     * If
     (
     Or
     (
     [{Client}File Name]="Defendant"
     ~[{Client}File Name]="Respondent"
     ~[{Client}File Name]="Insurer"
     )
     ~"Insurer"
     ~"Claimant"
     )
     */
    carsActingForClaimantOrInsurer: function () {
        var result = "";
        if (Client.FileName === "Defendant" || Client.FileName === "Respondent" || Client.FileName === "Insurer") {
            result = "Insurer"; ////TODO dependencies from other table rquires testing later
        }
        else {
            result = "Claimant";
        }
        return result;
    },
    //Text_Extract([CARS 2A 5A How is s91 Time Limits satisfied]~1~1)
    cars2a5aS91TimeLimitsSelection: function () {
        var result = "";
        if (this.cars2a5aHowIsS91TimeLimitsSatisfied) {
            result = this.cars2a5aHowIsS91TimeLimitsSatisfied[0];
        }

        return result;
    },
    //"<<"
    carsFlagText: function () {
        var result = "";
        result = "<<";
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="No"
     ~[CARS 2A 5A s91 Time Limits selection]="2"
     )
     ~[CARS flag text]
     ~""
     )
     *
     */
    cars2a5aFlag2S911b: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "No" && this.cars2a5aS91TimeLimitsSelection() === "2") {
            result = this.carsFlagText()
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="No"
     ~[CARS 2A 5A s91 Time Limits selection]="3"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5aFlag3S912: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "No" && this.cars2a5aS91TimeLimitsSelection() === "3") {
            result = this.carsFlagText()
        }
        return result;
    },
    //Text_Extract([CARS 2A 5A How is s91(2) satisfied]~1~1)
    cars2a5aS912Selection: function () {
        var result = "";
        if (this.cars2a5aHowIsS912Satisfied) {
            result = this.cars2a5aHowIsS912Satisfied[0];
        }
        return result;
    },
    /**
     * First_True
     (
     [CARS 2A 5B s91 Time Limits selection]="3"
     ~First_True
     (
     [CARS 2A 5B s91(2)(b) selection]="1"
     ~"Enter details of how s89A(3) applies"
     ~[CARS 2A 5B s91(2)(b) selection]="2"
     ~"Enter details of how s89C(4) applies"
     ~1=1
     ~""
     )


     ~[CARS 2A 5B s91 Time Limits selection]="5"
     ~"Enter details of how s91(2)(d) is satisfied"


     ~1=1
     ~"Not applicable"
     )
     */
    cars2a5bDetailsLabel: function () {
        var result = "";
        if (this.cars2a5bS91TimeLimitsSelection() === "3") {
            if (this.cars2a5bS912bSelection() === "1") {
                result = "Enter details of how s89A(3) applies";
            }
            else if (this.cars2a5bS912bSelection() === "2") {
                result = "Enter details of how s89C(4) applies";
            }
        }
        else if (this.cars2a5bS91TimeLimitsSelection() === "5") {
            result = "Enter details of how s91(2)(d) is satisfied";
        }
        else {
            result = "Not applicable";
        }
        return result;
    },
    //Text_Extract([CARS 2A 5B How is s91 Time Limits satisfied]~1~1)
    cars2a5bS91TimeLimitsSelection: function () {
        var result = "";
        if (this.cars2a5bHowIsS91TimeLimitsSatisfied) {
            result = this.cars2a5bHowIsS91TimeLimitsSatisfied[0];
        }
        return result;
    },
    //Text_Extract([CARS 2A 5B How is s91(2)(b) satisfied]~1~1)
    cars2a5bS912bSelection: function () {
        var result = "";
        if (this.cars2a5bHowIsS912bSatisfied) {
            result = this.cars2a5bHowIsS912bSatisfied[0];
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="1"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag1S911: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "1") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="2"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag2S912a: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "2") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="3"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag3S912b: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "3") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="4"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag4S912c: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "4") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="5"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag5S912d: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "5") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~Or
     (
     [CARS 2A 5B s91 Time Limits selection]="3"
     ~[CARS 2A 5B s91 Time Limits selection]="5"
     )
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlagDetails: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && (this.cars2a5bS91TimeLimitsSelection() === "3" || this.cars2a5bS91TimeLimitsSelection() === "5" )) {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="2"
     ~[CARS 2A 5B s91(2)(a) selection]="2"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a5bFlag2S89eb: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() == "Yes" && this.carsWasTheClaimMadeOnOrAfter1October2008() == "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "2"
            && this.cars2a5bS912aSelection() === "2") {
            result = this.carsFlagText();
        }
        return result;
    },
    //Text_Extract([CARS 2A 5B How is s91(2)(a) satisfied]~1~1)
    cars2a5bS912aSelection: function () {
        var result = "";
        if (this.cars2a5bHowIsS912aSatisfied) {
            result = this.cars2a5bHowIsS912aSatisfied[0];
        }
        return result;
    },
    /**
     * If
     (
     [CARS 2A 6 flagText]="Yes"
     ~[CARS flag text]
     ~""
     )
     */
    cars2a6FlagAssessmentDetails: function () {
        var result = "";
        if (this.cars2a6Flagtext() === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     Or
     (
     [CARS Which form Selection]="2A"
     ~And
     (
     [CARS Which form Selection]="2R"
     ~[CARS 2R is Q6 Information Correct]#"Yes"
     )
     )
     ~"Yes"
     ~"No"
     )
     */
    cars2a6Flagtext: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A"
            || (this.carsWhichFormSelection() === "2R" && this.cars2rIsQ6InformationCorrect !== "Yes")) {
            result = "Yes";
        }
        else {
            result = "No";
        }
        return result;
    },
    //"No"
    cars2a6aDefaultOfNo: function () {
        var result = "";
        result = "No";
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 6 flagText]="Yes"
     ~[CARS 2A 6A is Claim NOT suitable for assessment]="Yes"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a6aFlagYesFillAssessmentDetails: function () {
        var result = "";
        if (this.cars2a6Flagtext() === "Yes" && this.cars2a6aIsClaimNotSuitableForAssessment === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     [CARS 2A 6B Claim is Not ready selection]="1"
     ~[CARS 2A 6B When will Claim be ready for assessment]
     ~""
     )
     */
    cars2a6b1WhenWillClaimBeReadyForAssessment: function () {
        var result = "";
        if (this.cars2a6bClaimIsNotReadySelection() === "1") {
            result = this.cars2a6bWhenWillClaimBeReadyForAssessment;
        }
        return result;
    },
    //Text_Extract([CARS 2A 6B Reason claim is Not ready for assessment]~1~1)
    cars2a6bClaimIsNotReadySelection: function () {
        var result = "";
        if (this.cars2a6bReasonClaimIsNotReadyForAssessment) {
            result = this.cars2a6bReasonClaimIsNotReadyForAssessment[0];
        }
        return result;
    },
    /**
     * If
     (
     [CARS 2A 6B Claim is Not ready selection]="2"
     ~[CARS 2A 6B When will Claim be ready for assessment]
     ~""
     )
     */
    cars2a6b2WhenWillClaimBeReadyForAssessment: function () {
        result = "";
        if (this.cars2a6bClaimIsNotReadySelection() === "2") {
            result = this.cars2a6bWhenWillClaimBeReadyForAssessment;
        }
        return result;
    },
    /**
     * If
     (
     [CARS 2A 6B Claim is Not ready selection]="3"
     ~[CARS 2A 6B When will Claim be ready for assessment]
     ~""
     )
     */
    cars2a6b3WhenWillClaimBeReadyForAssessment: function () {
        var result = "";
        if (this.cars2a6bClaimIsNotReadySelection() === "3") {
            result = this.cars2a6bWhenWillClaimBeReadyForAssessment;
        }
        return result;
    },
    cars2a6bDefaultIsClaimReadyForAssessment: function () {
        var result = "";
        result = "Yes";
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 6 flagText]="Yes"
     ~[CARS 2A 6B is Claim ready for assessment]="No"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a6bFlagNoClaimIsNotReady: function () {
        var result = "";
        if (this.cars2a6Flagtext() === "Yes" && this.cars2a6bIsClaimReadyForAssessment === "No") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 6 flagText]="Yes"
     ~[CARS 2A 6B is Claim ready for assessment]="No"
     ~[CARS 2A 6B Claim is Not ready selection]="3"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a6bFlagOtherDetails: function () {
        var result = "";
        if (this.cars2a6Flagtext() === "Yes" && this.cars2a6bIsClaimReadyForAssessment === "No"
            && this.cars2a6bClaimIsNotReadySelection() === "3") {
            result = this.carsFlagText();
        }
        return result;
    },
    cars2a6cDefaultPrefLocation: function () {
        var result = "";
        result = "Sydney";
        return result;
    },
    cars2a7DefaultForLiability: function () {
        var result = "";
        result = "No";
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 7 flagText]="Yes"
     ~[CARS 2A 7 Is Liability an issue]="Yes"
     ~[CARS 2A 7 Liability issue selection]="1"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a7Flag1AllegedContribNeg: function () {
        var result = "";
        if (this.cars2a7Flagtext() === "Yes" && this.cars2a7IsLiabilityAnIssue === "Yes"
            && this.cars2a7LiabilityIssueSelection() === "1") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     Or
     (
     [CARS Which form Selection]="2A"
     ~And
     (
     [CARS Which form Selection]="2R"
     ~[CARS 2R is Q7 Information Correct]#"Yes"
     )
     )
     ~"Yes"
     ~"No"
     )
     */
    cars2a7Flagtext: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" ||
            (this.carsWhichFormSelection() === "2R" && this.cars2rIsQ7InformationCorrect !== "Yes")) {
            result = "Yes";
        }
        else {
            result = "No";
        }
        return result;
    },
    //Text_Extract([CARS 2A 7 Reason for liability issue]~1~1)
    cars2a7LiabilityIssueSelection: function () {
        var result = "";
        if (this.cars2a7ReasonForLiabilityIssue) {
            result = this.cars2a7ReasonForLiabilityIssue[0];
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 7 flagText]="Yes"
     ~[CARS 2A 7 Is Liability an issue]="Yes"
     ~[CARS 2A 7 Liability issue selection]="1"
     ~[CARS 2A 7 - 1 - does claimant agree they are at fault]="Yes"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a7Flag1YesClaimantAgrees: function () {
        var result = "";
        if (this.cars2a7Flagtext() === "Yes" && this.cars2a7IsLiabilityAnIssue === "Yes"
            && this.cars2a7LiabilityIssueSelection() === "1"
            && this.cars2a71DoesClaimantAgreeTheyAreAtFault === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 7 flagText]="Yes"
     ~[CARS 2A 7 Is Liability an issue]="Yes"
     ~[CARS 2A 7 Liability issue selection]="5"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a7Flag5OtherIssues: function () {
        var result = "";
        if (this.cars2a7Flagtext() === "Yes" && this.cars2a7IsLiabilityAnIssue === "Yes"
            && this.cars2a7LiabilityIssueSelection() === "5") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     [CARS 2A 7 flagText]="Yes"
     ~[CARS flag text]
     ~""
     )
     */
    cars2a7FlagLiabilityDetails: function () {
        var result = "";
        if (this.cars2a7Flagtext() === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS 2A 7 flagText]="Yes"
     ~[CARS 2A 7 Is Liability an issue]="Yes"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a7FlagYesLiabilityIsAnIssue: function () {
        var result = "";
        if (this.cars2a7Flagtext() === "Yes" && this.cars2a7IsLiabilityAnIssue === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     Or
     (
     [CARS Which form Selection]="2A"
     ~[CARS 2R 8 Dispute Info flag ReplyText]="Yes"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8FlagReplyDisputeInformation: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" || this.cars2r8DisputeInfoFlagReplytext() === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2R"
     ~[CARS Reply - is Dispute Information Correct]#"Yes"
     )
     ~"Yes"
     ~"No"
     )
     */
    cars2r8DisputeInfoFlagReplytext: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2R" && this.carsReplyIsDisputeInformationCorrect !== "Yes") {
            result = "Yes";
        }
        else {
            result = "No";
        }
        return result;
    },
    //Text_Extract([CARS 2A 8a What is agreed for non-economic loss]~1~1)
    cars2a8aAgreedLossSelection: function () {
        var result = "";
        if (this.cars2a8aWhatIsAgreedForNoneconomicLoss) {
            result = this.cars2a8aWhatIsAgreedForNoneconomicLoss[0];
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2A"
     ~[CARS 2A 8a Selection]="1"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlag1AgreementApplies: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" && this.cars2a8aSelection() === "1") {
            result = this.carsFlagText();
        }
        return result;
    },
    //Text_Extract([CARS 2A 8a How is Cl 9_3 satisfied]~1~1)
    cars2a8aSelection: function () {
        var result = "";
        if (this.cars2a8aHowIsCl93Satisfied) {
            result = this.cars2a8aHowIsCl93Satisfied[0];
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2A"
     ~Or
     (
     [CARS 2A 8a Selection]="2"
     ~[CARS 2A 8a Selection]="3"
     )
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlagAssessorName: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" && (this.cars2a8aSelection() === "2"
            || this.cars2a8aSelection() === "3")) {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2A"
     ~Or
     (
     [CARS 2A 8a Selection]="2"
     ~[CARS 2A 8a Selection]="3"
     )
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlagCertificateDate: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" && (this.cars2a8aSelection() === "2"
            || this.cars2a8aSelection() === "3")) {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2A"
     ~[CARS 2A 8a Selection]="4"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlagDateMasAppLodged: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" && this.cars2a8aSelection() === "4") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Which form Selection]="2A"
     ~Or
     (
     [CARS 2A 8a Selection]="2"
     ~[CARS 2A 8a Selection]="3"
     ~[CARS 2A 8a Selection]="4"
     )
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlagMasMatterNo: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A" && (this.cars2a8aSelection() === "2" ||
            this.cars2a8aSelection() === "3" || this.cars2a8aSelection() === "4")) {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     [CARS Which form Selection]="2A"
     ~[CARS flag text]
     ~""
     )
     */
    cars2a8aFlagNoneconomicLoss: function () {
        var result = "";
        if (this.carsWhichFormSelection() === "2A") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="No"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2aFlagClaimBefore1October2008: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes"
            && this.carsWasTheClaimMadeOnOrAfter1October2008() === "No") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS flagText Time Limit Information]="Yes"
     ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2aFlagClaimOnOrAfter1October2008: function () {
        var result = "";
        if (this.carsFlagtextTimeLimitInformation() === "Yes"
            && this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes") {
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="3"
     ~[CARS 2A 5B s91(2)(b) selection]="1"
     )
     ~[CARS 2A 5B Details]
     ~""
     )
     */
    cars2aS89a3Details: function () {
        var result = "";
        if (this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "3"
            && this.cars2a5bS912bSelection() === "1") {
            result = this.cars2a5bDetails;
        }
        return result;
    },
    /**
     * If
     (
     And
     (
     [CARS Was the Claim made on or after 1 October 2008]="Yes"
     ~[CARS 2A 5B s91 Time Limits selection]="3"
     ~[CARS 2A 5B s91(2)(b) selection]="2"
     )
     ~[CARS 2A 5B Details]
     ~""
     )
     */
    cars2aS89c4Details: function () {
        var result = "";
        if (this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"
            && this.cars2a5bS91TimeLimitsSelection() === "3"
            && this.cars2a5bS912bSelection() === "2") {
            result = this.cars2a5bDetails;
        }
        return result;
    },
    /**
     * If
     (
        And
        (
         [CARS Was the Claim made on or after 1 October 2008]="Yes"
         ~[CARS 2A 5B s91 Time Limits selection]="5"
        )
     ~[CARS 2A 5B Details]
     ~""
     )
     */
    cars2aS912dDetails:function () {
        var result = "";
        if(this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes" &&
        this.cars2a5bS91TimeLimitsSelection() === "5"){
            result = this.cars2a5bDetails;
        }
        return result;
    },
    /**
     * If
     (
        [CARS 2R 8 Dispute Info flag ReplyText]="Yes"
        ~[CARS flag text]
        ~""
     )
     */
    cars2r8aFlagNoneconomicLoss: function () {
        var result = "";
        if(this.cars2r8DisputeInfoFlagReplytext() === "Yes"){
            result = this.carsFlagText();
        }
        return result;
    },
    /**
     * If
     (
         And
         (
            [CARS 2R 8 Dispute Info flag ReplyText]="Yes"
         ~Or
         (
            [CARS 2R 8a Reply Is info correct selection]="2"
            ~[CARS 2R 8a Reply Is info correct selection]="3"
         )
     )
     ~[CARS flag text]
     ~""
     )
     */
    cars2r8aFlagReplyIncorrectInfo: function () {
        var result = "";
        if(this.cars2r8DisputeInfoFlagReplytext() === "Yes" &&
            (this.cars2r8aReplyIsInfoCorrectSelection() === "2"
            ||this.cars2r8aReplyIsInfoCorrectSelection() === "3"))
        {
            result = this.carsFlagText();
        }
        return result;
    },
    //Text_Extract([CARS 2R 8a Reply is info correct]~1~1)
    cars2r8aReplyIsInfoCorrectSelection:function () {
        var result = "";
        if (this.cars2r8aReplyIsInfoCorrect) {
            result = this.cars2r8aReplyIsInfoCorrect[0];
        }
        return result;
    },
    /**
     * If
     (
        [CARS 2R 8 Dispute Info flag ReplyText]="Yes"
     ~First_True
     (
        [CARS 2R 8a Reply Is info correct selection]="2"
        ~"Provide details of how Clause 9.3 is satisifed"

        ~[CARS 2R 8a Reply Is info correct selection]="3"
        ~"Provide details why Clause 9.3 is not satisifed"
     )
     ~"not applicable"
     )
     */
    cars2r8aReplyDetailsLabel:function () {
        var result = "";
        if(this.cars2r8DisputeInfoFlagReplytext() === "Yes"
        && this.cars2r8aReplyIsInfoCorrectSelection() === "2"){
            result = "Provide details of how Clause 9.3 is satisifed";
        }
        else if(this.cars2r8DisputeInfoFlagReplytext() === "Yes"
            && this.cars2r8aReplyIsInfoCorrectSelection() === "3"){
            result = "Provide details why Clause 9.3 is not satisifed";
        }
        else {
            result = "not applicable";
        }
        return result;
    },
    /**
     * If
     (
         And
         (
             [CARS Which form Selection]="5A"
             ~[CARS Was the Claim made on or after 1 October 2008]="No"
         )
     ~[CARS flag text]
     ~""
     )
     */
    cars5aFlagClaimBefore1October2008:function () {
        var result = "";
        if(this.carsWhichFormSelection() === "5A" &&
        this.carsWasTheClaimMadeOnOrAfter1October2008() === "No"){
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
         And
         (
           [CARS Which form Selection]="5A"
             ~[CARS Was the Claim made on or after 1 October 2008]="Yes"
         )
     ~[CARS flag text]
     ~""
     )
     */
    cars5aFlagClaimOnOrAfter1October2008:function () {
        var result = "";
        if(this.carsWhichFormSelection() === "5A" &&
            this.carsWasTheClaimMadeOnOrAfter1October2008() === "Yes"){
            result = this.carsFlagText();
        }
        return result;
    },
    //Text_Extract([CARS 5A Q6A Dispute Information]~1~1)
    cars5aQ6aDisputeSelection:function () {
        var result = "";
        if (this.cars5aQ6aDisputeInformation) {
            result = this.cars5aQ6aDisputeInformation[0];
        }
        return result;
    },
    //Text_Extract([CARS 5A Q6B Dispute Information]~1~1)
    cars5aQ6bDisputeSelection:function () {
        var result = "";
        if (this.cars5aQ6bDisputeInformation) {
            result = this.cars5aQ6bDisputeInformation[0];
        }
        return result;
    },
    //Text_Extract([CARS Who is the Claim made against]~1~1)
    carsClaimIsAgainstSelection:function () {
        var result = "";
        if (this.carsWhoIsTheClaimMadeAgainst) {
            result = this.carsWhoIsTheClaimMadeAgainst[0];
        }
        return result;
    },
    carsDefaultAreYouCompletingAReplyForm:function () {
        var result = "";
        result = "No";
        return result;
    },
    carsDefaultOfYes: function () {
        var result = "";
        result = "Yes";
        return result;
    },
    carsDefaultWhoIsTheClaimMadeAgainst: function () {
        var result = "";
        result = "1 - a NSW CTP Insurer";
        return result;
    },
    /*
     First_True
     (
        Or
         (
            [CARS Which form Selection]="2A"
            ~[CARS Which form Selection]="2R"
         )
        ~"The list of documents for Q9 must be entered in the document ."


        ~Or
         (
            [CARS Which form Selection]="5A"
            ~[CARS Which form Selection]="5R"
         )
        ~"The list of documents for Q7 must be entered in the document ."

     )
     */
    carsDocumentInformationQuestion: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2A" ||
            this.carsWhichFormSelection() === "2R"){
            result = "The list of documents for Q9 must be entered in the document .";
        }
        else if(this.carsWhichFormSelection() === "5A"
        || this.carsWhichFormSelection() === "5R"){
            result = "The list of documents for Q7 must be entered in the document .";
        }
        return result;
    },
    /*
     If
     (
        [CARS Acting for Claimant or Insurer]="Claimant"
        ~[CARS flag text]
        ~""
     )
     */
    carsFlagActForClaimant: function () {
        var result = "";
        if(this.carsActingForClaimantOrInsurer() === "Claimant"){
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
         [CARS Acting for Claimant or Insurer]="Insurer"
         ~[CARS flag text]
         ~""
     )
     */
    carsFlagActForInsurer: function () {
        var result = "";
        if(this.carsActingForClaimantOrInsurer() === "Insurer"){
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
         Or
         (
            [CARS Which form Selection]="2A"
            ~[CARS Which form Selection]="5A"
             ~And
             (
                [CARS Reply is being completed]="Yes"
                ~[CARS Reply - is Insurer Information Correct]#"Yes"
             )
        )
     ~[CARS flag text]
     ~""
     )
     */
    carsFlagInsurerInformation: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2A" ||
        this.carsWhichFormSelection() === "5A" ||
            (this.carsReplyIsBeingCompleted() === "Yes" &&
            this.carsReplyIsInsurerInformationCorrect !== "Yes")){
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     First_True
     (
         Or
         (
            [CARS Which form Selection]="2R"
            ~[CARS Which form Selection]="5R"
         )
     ~"Yes"
     ~"No"
     )
     */
    carsReplyIsBeingCompleted: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2R" ||
        this.carsWhichFormSelection() === "5R"){
            result = "Yes";
        }
        else {
            result = "No";
        }
        return result;
    },
    /*
     If
     (
     [CARS Which form Selection]="2R"
     ~[CARS flag text]
     ~""
     )
     */
    carsFlagReplyForm2rYes: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2R")
        {
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
         Or
         (
             [CARS Which form Selection]="2R"
            ~[CARS Which form Selection]="5R"
         )
     ~[CARS flag text]
     ~""
     )
     */
    carsFlagReplyFormYes: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2R" ||
            this.carsWhichFormSelection() === "5R")
        {
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
        [CARS flagText Time Limit Information]="Yes"
        ~[CARS flag text]
        ~""
     )
     */
    carsFlagTimeLimitInformation: function () {
        var result = "";
        if(this.carsFlagtextTimeLimitInformation() === "Yes"){
            result = this.carsFlagText();
        }
        return result;
    },
    /*
     If
     (
         And
         (
            [CARS Which form Selection]="2R"
            ~[CARS 2R is Q6 Information Correct]="Yes"
         )
     ~""
     ~[CARS flag text]
     )
     */
    carsPreferredLocationFlag: function () {
        var result = "";
        if(this.carsWhichFormSelection() === "2R" &&
        this.cars2rIsQ6InformationCorrect === "Yes"){
            result = "";
        }
        else {
            result = this.carsFlagText();
        }
        return result;
    },
    //"CARS Form "+[CARS Which form are you completing]
    display: function () {
        var result = "";
        result = "CARS Form " + this.carsWhichFormAreYouCompleting;
        return result;
    }
}




















