var Tribunal = {
    amtPerFortnight1: 10,
    amtPerFortnight2: 11,
    amtPerFortnight3: 12,
    dateOfDecision: new Date(1993, 6, 28, 14, 39, 7),
    rentBoardMortgage : 10,
    creditCardHpLoanRepayments: 10,
    livingExpFoodClothingEtc: 10,
    expFaresGasElecEtc: 10,
    otherPerFtAmt1: 10,
    otherPerFtAmt2: 10,
    netIncomePerFortnight: 15,
    AmtPerFtOtherIncomeTotal: function(){
        var result = 0;
        result = (this.amtPerFortnight1 + this.amtPerFortnight2 + this.amtPerFortnight3).currency();
        return result;
    },
    //"Date of Decision: "+[Date of decision]
    DisplayAdt: function(){
// AdminTribunalForms.displayAdt. JS Calculation created on 25/07/2016 by GZ
        var result = '';
        result = "Date of Decision: " + this.dateOfDecision.format("dd mmmm, yyyy")
        return result;
    },
    //[Amt per fortnight 1]+[Amt per fortnight 2]+[Amt per fortnight 3]
    totalAmtPerFtOtherIncome: function(){
        var result = '';
        result = this.amtPerFortnight1 + this.amtPerFortnight2 + this.amtPerFortnight3;
        result = result.currency();
        return result;
    },
    //[Rent/Board/Mortgage]+[Credit card, HP, loan repayments]+[Living exp food, clothing etc]+[exp fares, gas, elec etc]+[Other per ft amt 1]+[Other per ft amt 2]
    TotalExp:function(){
        return (this.rentBoardMortgage + this.creditCardHpLoanRepayments + this.livingExpFoodClothingEtc + this.expFaresGasElecEtc + this.otherPerFtAmt1 + this.otherPerFtAmt2).currency();
    },
    //[Net income per fortnight]+[Total amt per ft other income]
    NetIncomePerFortnight: function(){
        // AdminTribunalForms.totalIncome. JS Calculation created on 25/07/2016 by GZ
        // This function has dependency on other functions~!!
        return (this.netIncomePerFortnight + parseFloat(this.totalAmtPerFtOtherIncome().substring(1))).currency();
    }
}

