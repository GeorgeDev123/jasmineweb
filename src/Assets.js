/**
 * Created by George.Zheng on 4/08/2016.
 */
var Asset = {
    assetType: "Cash",
    description: "Cash Description",
    deceasedsShareField: 10,
    ifTenantsInCommonHowManyShares: 10,
    saRetentionNoOfMonths: 10,
    saRetentionAmount: 100,
    saAmountOriginalBond: 2000,
    saPortionOfBondUnpaid: 10,
    saAccomBondInterest: 10,
    saAccomBondPrepaidMaintenance: 100,
    saAccomBondPersonalAccount: 10,
    valueAtDeath: 100,
    interestAccrued: 100,
    //[Asset Type]+If([Description]=""~""~ASCII_Character(11)+[Description])
    ainvGeneralCellDesc: function () {
        var result = "";
        result += this.assetType;
        if (this.description) {
            result += String.fromCharCode(11) + this.description;
        }
        return result;
    },
    /*
     Multiple_Add_test
     (
     [Owned]="Tenants in Common in equal shares"
     ~[AInv General cell Desc]
     ~"

     "
     ~"

     "
     ~*
     ).

     */
    ainvEqualCellDesc: function () {
        var result = '';
        result = matter.assetsList.map(function (asset) {
            if (asset.owned === "Tenants in Common in equal shares") {
                return asset.ainvGeneralCellDesc();
            }
        }).join(String.fromCharCode(13) + String.fromCharCode(13));
        return result;
    },
    deceasedsShareCalc: function () {
        var result = 0;
        if (matter.state === "SA" && this.assetType.includes("Accommodation Bond")) {
            result = this.saAccomBondTotalRefund();
        } else if (this.deceasedsShareField > 0) {
            result = this.deceasedsShareField.currency();
        } else if (this.ifTenantsInCommonHowManyShares > 0) {
            result = (numeral().unformat(this.totalValuePlusInterest()) / this.ifTenantsInCommonHowManyShares).currency();
        } else {
            result = this.totalValuePlusInterest();
        }
        return result;
    },
    totalValuePlusInterest: function () {
        var result = 0;
        result = this.valueAtDeath + this.interestAccrued;
        return result.currency();
    },
    //[SA Accom Bond Total Repaid]+[SA Accom Bond Prepaid Maintenance]+[SA Accom Bond Personal Account]
    saAccomBondTotalRefund: function () {
        // Assets.saAccomBondTotalRefund. JS Calculation created on 4/08/2016 by GZ
        var result = 0;
        result = numeral().unformat(this.saAccomBondTotalRepaid()) + this.saAccomBondPrepaidMaintenance + this.saAccomBondPersonalAccount;
        return result.currency();
    },
    //[SA Amount Original Bond]-[SA Portion of Bond unpaid]-[SA Retention Total Amount]-[SA Accom Bond Interest]
    saAccomBondTotalRepaid: function () {
        // Assets.saAccomBondTotalRepaid. JS Calculation created on 4/08/2016 by GZ
        var result = 0;
        result = this.saAmountOriginalBond - this.saPortionOfBondUnpaid - numeral().unformat(this.saRetentionTotalAmount()) - this.saAccomBondInterest;
        return result.currency();
    },
    //[SA Retention no of months]*[SA Retention amount]
    saRetentionTotalAmount: function () {
        // Assets.saRetentionTotalAmount. JS Calculation created on 4/08/2016 by GZ
        var result = 0;
        result = this.saRetentionNoOfMonths * this.saRetentionAmount;
        return result.currency();
    },
    /*
     First_True
     (
     And
     (
     Text_Position
     (
     [SA All asset types listed for inv]
     ~"Accommodation Bond in SA"
     )>0
     ~Text_Position
     (
     [{Assets Held}SA All asset types listed for inv]
     ~"Accommodation Bond in SA"
     )>0
     )
     ~"Both"


     ~Text_Position
     (
     [SA All asset types listed for inv]
     ~"Accommodation Bond in SA"
     )>0
     ~"Assets"


     ~Text_Position
     (
     [{Assets Held}SA All asset types listed for inv]
     ~"Accommodation Bond in SA"
     )>0
     ~"Assets Held"


     ~1=1
     ~"No"
     )
     */
    saCheckAccommodationBondInSa:function () {
        // Assets.saCheckAccommodationBondInSa. JS Calculation created on 5/08/2016 by GZ
        var result = '';
        if (this.saAllAssetTypesListedForInv().indexOf("Accommodation Bond in SA") !== -1 &&
            matter.assetsHeld.saAllAssetTypesListedForInv().indexOf("Accommodation Bond in SA")) {
            result = "Both";
        } else if (this.saAllAssetTypesListedForInv().indexOf("Accommodation Bond in SA") !== -1) {
            result = "Assets";
        } else if (matter.assetsHeld.saAllAssetTypesListedForInv().indexOf("Accommodation Bond in SA") !== -1) {
            result = "Assets Held";
        } else {
            result = "No";
        }
        return result;
    },
    /*
     Multiple_Add_Test
     (
     [Include in Affidavit?]="Yes"
     ~[Asset Type]+If
     (
     [Location]="South Australia"
     ~" in SA"
     ~" outside SA"
     )
     ~ASCII_Character
     (
     13
     )
     ~ASCII_Character
     (
     13
     )
     ~*
     )
     */
    saAllAssetTypesListedForInv: function () {
        
    },
    /**
     * "Original accommodation bond"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Amount Original Bond]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Less portion unpaid"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Portion of Bond unpaid]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Less retention "+[SA Retention no of months]+" months @ "+Format
     (
     [SA Retention amount]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     9
     )+Format
     (
     [SA Retention Total Amount]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Less interest"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Accom Bond Interest]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Total to be repaid"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Accom Bond Total Repaid]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Plus prepaid maintenance fees"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Accom Bond Prepaid Maintenance]
     ~"$###,###,##0.00"
     )+ASCII_Character
     (
     11
     )+"Plus balance of personal account"+ASCII_Character
     (
     9
     )+Format
     (
     [SA Accom Bond Personal Account]
     ~"$###,###,##0.00"
     )
     */
    saAccomBondSetOutCalc:function () {
        
    },
    saDeceasedAddress: function () {
        
    },
    saSourceCalc:function () {
        
    }
}