var Property = {
    property: property = {
    streetNo: 1},
    property1: property1 = {
    streetNo: 2},
    matter: matter = {
        propertyList: [this.property, this.property1]
    },
    volume: "13245",
    folio: "123",
    state: "NSW",
    titleReference: "Title reference",
    conveyanceNo: "123",
    book: "book",
    returnProperties: function(){
        var result="";
        if(this.matter.propertyList[1].streetNo){
            result = "properties";
        }
        else{
            result = "a property";
        }
        return result;
    },
    actVolume: function(){
        var result="";
        result += this.volume + ":" + this.folio;
        return result;
    },
    volFolConveyBkSingle: function(){
        var result ="";
        if(this.volume){
            result ="Volume" + this.volume + "Folio" + this.folio;
        }
        else if(this.conveyanceNo){
            result = "Conveyance No. " + this.conveyanceNo + " Book " + this.book;
        }
        return result;
    },
    propertyAddressAcross:function () {
        return "propertyAddressAcrossString";
    },
    volumeAndFolio: function () {
        var result = "";
        result = "Volume " + this.volume + " Folio " + this.folio;
        return result;
    }
    // certOfTitleDescript: function () {
    //     var result="";
    //     if(this.state === "NSW" || this.state === "QLD" || this.state === "Q"){
    //         result = this.titleReference;
    //     }
    //     if(this.state === "VIC"){
    //         result = this.volFolConveyBkSingle();
    //     }
    //     if(this.state === "ACT"){
    //         result = this.
    //     }
    // }
}