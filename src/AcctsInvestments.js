/**
 * Created by George.Zheng on 3/08/2016.
 */
var Accts = {
    //Sum([Bank current balance]~*)
    aTotal:function () {
        var result = 0;
        if (matter.acctsInvestmentsList) {
            matter.acctsInvestmentsList.forEach(function(act) {
                result += act.bankCurrentBalance;
            });
        }
        return result.currency();
    },
    /**
     * Multiple_Add([Bank account number]~"
     "~"
     "~*)
     * @returns {string}
     */
    bankAccountNoList: function () {
        // AcctsInvestments.bankAccountNoList. JS Calculation created on 3/08/2016 by GZ
        var result = '';
        if (matter.acctsInvestmentsList) {
            matter.acctsInvestmentsList.forEach(function(act, idx, acctList) {
                if (idx !== acctList.length - 1) {
                    result += act.bankAccountNumber + "\r";
                } else {
                    // Last element in the array
                    result += act.bankAccountNumber;
                }
            });
        }
        return result;
    },
    bankAddressList: function () {
        // AcctsInvestments.bankAddressList. JS Calculation created on 3/08/2016 by GZ
        var result = '';
        if (matter.acctsInvestmentsList) {
            matter.acctsInvestmentsList.forEach(function(act, idx, acctList) {
                if (act.bankAddress && idx !== acctList.length - 1) {
                    result += act.bankAddress + "\r";
                } else if (act.bankAddress) {
                    // Last element in the array
                    result += act.bankAddress;
                }
            });
        }
        return result;
    }
}