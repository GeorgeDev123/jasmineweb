/**
 * Created by George.Zheng on 27/07/2016.
 */
var LEAP ={
    utilities:{
        isDate: function(obj){
            return Object.prototype.toString.call(obj) === '[object Date]' && !isNaN(obj.valueOf()) ;
        },
        date: {
            isValid: function (date) {
                return isValid(date);
            },
            calculateAge: function(birthday, referenceDate){
                birthday = parse(birthday);
                referenceDate = parse(referenceDate) || new Date();

                if (!isValid(birthday)) {
                    return 0;
                }


                // Check the reference date is a valid date
                if (!isValid(referenceDate)) {
                    referenceDate = new Date(); // Reference date is invalid, so use today.
                }

                return yearsBetween(birthday, referenceDate);
            },
            daysBetween: function (startDate, endDate) {
              return daysBetween(startDate, endDate);
            },
            yearsBetween: function (startDate, endDate) {
                //startDate = parse(startDate);
                //endDate = parse(endDate);

                if (!isValid(startDate) || !isValid(endDate)) {
                    return 0;
                }

                if(startDate > endDate) {
                    var tempDate = startDate;
                    startDate = endDate;
                    endDate = tempDate;
                }

                var years = endDate.getFullYear() - startDate.getFullYear();
                var m = endDate.getMonth() - startDate.getMonth();
                if (m < 0 || (m === 0 && endDate.getDate() < startDate.getDate())) {
                    years--;
                }
                return years;
            },
            daysInMonth: function (year, month) {
                return [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
            },
            isLeapYear: function (year) {
                return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
            }
        },
        toWords: function (s) {
            var th = ['','thousand','million', 'billion','trillion'];
            var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
            var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
            var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

            s = s.toString();
            s = s.replace(/[\, ]/g,'');
            if (s != parseFloat(s)) return 'not a number';
            var x = s.indexOf('.');
            if (x == -1) x = s.length;
            if (x > 15) return 'too big';
            var n = s.split('');
            var str = '';
            var sk = 0;
            for (var i=0; i < x; i++)
            {
                if ((x-i)%3==2)
                {
                    if (n[i] == '1')
                    {
                        str += tn[Number(n[i+1])] + ' ';
                        i++;
                        sk=1;
                    }
                    else if (n[i]!== 0)
                    {
                        str += tw[n[i]-2] + ' ';
                        sk=1;
                    }
                }
                else if (n[i]!== 0)
                {
                    str += dg[n[i]] +' ';
                    if ((x-i)%3===0) str += 'hundred ';
                    sk=1;
                }

                if ((x-i)%3==1)
                {
                    if (sk) str += th[(x-i-1)/3] + ' ';
                    sk=0;
                }
            }
            if (x != s.length)
            {
                var y = s.length;
                str += 'point ';
                for (var z=x+1; z<y; z++) str += dg[n[z]] +' ';
            }
            return str.replace(/\s+/g,'');
        }
    }
}
function yearsBetween(startDate, endDate) {
    startDate = parse(startDate);
    endDate = parse(endDate);

    if (!isValid(startDate) || !isValid(endDate)) {
        return 0;
    }

    if (startDate > endDate) {
        var tempDate = startDate;
        startDate = endDate;
        endDate = tempDate;
    }

    var years = endDate.getFullYear() - startDate.getFullYear();
    var m = endDate.getMonth() - startDate.getMonth();
    if (m < 0 || (m === 0 && endDate.getDate() < startDate.getDate())) {
        years--;
    }
    return years;
}

function isValid(theDate) {
    theDate = parse(theDate);
    //return Object.prototype.toString.call(theDate) === '[object Date]';
    return LEAP.utilities.isDate(theDate);
}
function parse(theDate) {
    if (theDate instanceof Date) {
        return theDate;
    }
    else {
        return null;
    }

    if (matchesDatePattern(theDate)) {
        // Note that the Date.parse function has been overridden by Date.js
        // and no longer handles ISO formatted dates.
        if (isoPattern.exec(theDate)) {
            if(!timezonePattern.exec(theDate)) {
                theDate += new Date().getUTCOffset().splice(3,0,':');
            }

            if(invalidTimezonePattern.exec(theDate)) {
                theDate = theDate.splice(22,0,':');
            }

            theDate = new Date(theDate);
        } else {
            theDate = Date.parse(theDate);
        }
        if (isNaN(theDate)) {
            return null;
        }
        return theDate;
    }
    return null;
}
function daysBetween(d1, d2) {
    d1 = parse(d1) || 0;
    d2 = parse(d2) || 0;

    var startDate = d1 <= d2 ? d1 : d2;
    var endDate = d2 >= d1 ? d2 : d1;

    if (isValid(startDate) && isValid(endDate)) {
        //startDate.clearTime();
        //endDate.clearTime();

        // The number of milliseconds in one day
        var ONE_DAY = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var d1_ms = startDate.getTime();
        var d2_ms = endDate.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = Math.abs(d1_ms - d2_ms);

        // Convert back to days and return
        return Math.round(difference_ms / ONE_DAY);
    }
    return 0;
}
Number.prototype.currency = function(c, d, t){
    var n = this,
        c = c == undefined ? 2 : c
    d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return "$" + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var	_ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};