/**
 * Created by George.Zheng on 27/07/2016.
 */
var Lease = {
    contributionAmount: 100,
    contributionOfBaseRent: 10,
    contributionAdditionalRentYesNo: "Yes",
    leaseCommencementDate: new Date(2016, 01, 01, 0, 0, 0),
    leaseTerminationDate: new Date(2016, 03, 01, 0, 0, 0),
    titleReferenceOfProperty: "propertyReferenceTitle",
    propertyBuildingName: "GeorgeBuilding",
    flatUnitShop: "Unit 1",
    streetNo: "1",
    street: "George St",
    suburb: "Docklands",
    premiumRetirementVillage: "Yes",
    premium: 100,
    termOfLease: "1 year",
    duplicateDocuments: 1,
    /*
     First_True
     (
        [contribution amount]>0
        ~[{Lease Details}contribution amount]


        ~[contribution % of base rent]>0
        ~(

         (
            [{Rent}Initial Rent p.a.]*[contribution % of base rent]
            )/100
         )


     ~1=1
     ~0
     )
     */
    contributionPromLevyBaseAmt: function () {
        var result = 0;
        if (this.contributionAmount > 0) {
            result = this.contributionAmount;
        } else if (this.contributionOfBaseRent > 0) {
            result = (matter.rent.initialRentPa * this.contributionOfBaseRent / 100);
        }
        return result.currency();
    },
    /*
     If
     (
     [{Lease Details}Contribution Additional Rent (Yes/No)]="Yes"
     ~[{Lease Details}Contribution Prom Levy BASE AMT]
     ~0
     )
     */
    contributionPromSdLevyBaseAmt: function () {
        var result = 0;
        result = result.currency();
        if(this.contributionAdditionalRentYesNo === "Yes"){
            result =  this.contributionPromLevyBaseAmt();
        }
        return result;
    },
    /*
     First_True
     (
     [contribution amount]>0
     ~
     (
     [{Lease Details}contribution amount]*[{Lease Details}Term (years)]
     )+
     (
     [{Lease Details}contribution amount]/12*[{Lease Details}Term (months)]
     )+
     (
     [{Lease Details}contribution amount]/365.25*[{Lease Details}Term (days)]
     )
     ~[contribution % of base rent]>0
     ~
     (

     (

     (
     [{Rent}Initial Rent p.a.]*[contribution % of base rent]
     )/100
     )*[{Lease Details}Term (years)]
     )+
     (

     (

     (
     [{Rent}Initial Rent p.a.]*[contribution % of base rent]
     )/100
     )/12*[{Lease Details}Term (months)]
     )+
     (

     (

     (
     [{Rent}Initial Rent p.a.]*[contribution % of base rent]
     )/100
     )/365.25*[{Lease Details}Term (days)]
     )
     ~1=1
     ~0
     )
     */
    contributionPromotionLevyCalc: function () {
        var result = 0;
        if(this.contributionAmount > 0){
            result = this.contributionAmount * this.termYears() +
                this.contributionAmount /12 * this.termMonths() +
                this.contributionAmount /365.25 * this.termDays();
        }
        else if(this.contributionOfBaseRent > 0) {
            var rentContribution = matter.rent.initialRentPa * this.contributionOfBaseRent / 100;
            result = rentContribution * this.termYears() +
                rentContribution / 12 * this.termMonths() +
                rentContribution / 365.25 * this.termDays();
        }
        result = result.currency();
        return result;
    },
    /*
     Years_Between
     (
         [Lease Commencement Date]
         ~[Lease Termination Date]
         ~*
     )
     */
    termYears:function () {
        var result = 0;
        result = LEAP.utilities.date.yearsBetween(this.leaseCommencementDate, this.leaseTerminationDate);
        return result;
    },
    termMonths: function () {
        var result = 0;
        if (!LEAP.utilities.date.isValid(this.leaseCommencementDate) || !LEAP.utilities.date.isValid(this.leaseTerminationDate) ||
            this.leaseTerminationDate < this.leaseCommencementDate ||
            LEAP.utilities.date.daysBetween(this.leaseCommencementDate, this.leaseTerminationDate) < 30) {
            return result;
        }
        if (LEAP.utilities.date.yearsBetween(this.leaseCommencementDate, this.leaseTerminationDate) < 1) {
            if (this.leaseTerminationDate.getMonth() - this.leaseCommencementDate.getMonth() < 0) {
                result = this.leaseTerminationDate.getMonth() - this.leaseCommencementDate.getMonth() + 12;
            }
            else {
                result = this.leaseTerminationDate.getMonth() - this.leaseCommencementDate.getMonth();
            }

        }
        else {
            result = (this.leaseTerminationDate.getMonth() - this.leaseCommencementDate.getMonth() +
                (12 * LEAP.utilities.date.yearsBetween(this.leaseCommencementDate, this.leaseTerminationDate)))
                % 12;
        }
        return result;
    },
    //Months_Between([Lease Commencement Date]~[Lease Termination Date]~*)
    termDays: function(){
        var result = 0;

        if (!LEAP.utilities.date.isValid(this.leaseCommencementDate) || !LEAP.utilities.date.isValid(this.leaseTerminationDate) ||
                this.leaseTerminationDate < this.leaseCommencementDate) {
            return result;
        }
        if(this.leaseTerminationDate.getDate() === this.leaseCommencementDate.getDate()) {
            result += 1;
        }
        else if(this.leaseTerminationDate.getDate() > this.leaseCommencementDate.getDate()){
            result = this.leaseTerminationDate.getDate() - this.leaseCommencementDate.getDate() + 1;
        }
        else {
            result = LEAP.utilities.date.daysInMonth(this.leaseCommencementDate.getFullYear(), this.leaseCommencementDate.getMonth()
                ) - this.leaseCommencementDate.getDate() + this.leaseTerminationDate.getDate() + 1;
        }
        return result;
    },
    //[{Rent}Set Stamp Duty Values]+Lease_S_D_End_Date(1)
    endDate1: function () {
        
    },
    //"Commencement Date: "+[Lease Commencement Date]+
    // " Termination Date: "+[Lease Termination Date]
    layoutTitle: function () {
        var result = "";
        result = "Commencement Date: "+ this.leaseCommencementDate.toString("dd MMMM, yyyy") +
            " Termination Date: "+ this.leaseTerminationDate.toString("dd MMMM, yyyy");
        return result;
    },
    /*
     "TITLE REFERENCE OF PROPERTY:   "+
     First_True
     (
         [Property Description - across]#""
         ~[Title Reference of Property]
         ~1=1
         ~[{Property}Title Reference]
     )
     +"
     "+"DESCRIPTION OF PROPERTY:   "+
     First_True
     (
         [Property Description - across]#""
         ~[Property Description - across]
         ~1=1
         ~[{Property}Property Address (across)]
     )
     */
    premisesDetailsCalc: function () {
        var result = ""
        if(this.propertyDescriptionAcross()){
            result = "TITLE REFERENCE OF PROPERTY: " + this.titleReferenceOfProperty + "\n" +
                "DESCRIPTION OF PROPERTY: " + this.propertyDescriptionAcross();
        }
        else{
            result = "TITLE REFERENCE OF PROPERTY: " +  matter.property.titleReference + "\n" +
                "DESCRIPTION OF PROPERTY: " + matter.property.propertyAddressAcross();
        }

        return result;
    },
    /*
     If
     (
         [Property/Building Name]=""
         ~""
         ~If
         (
             [Flat/Unit/Shop]#""
             ~[Flat/Unit/Shop]+", "+[Property/Building Name]+", "
             ~[Property/Building Name]+" "
         )
         )
         +If
         (
             And
             (
                 [Flat/Unit/Shop]#""
                 ~[Property/Building Name]#""
             )
         ~""
         ~If
         (
             [Flat/Unit/Shop]#""
             ~[Flat/Unit/Shop]+"/"
             ~""
         )
         )+if
         (
             [Street No.]=""
             ~""
             ~[Street No.]+" "
         )+if
         (
             [Street]=""
             ~""
             ~[Street]+", "
         )+[Suburb]
     */
    propertyDescriptionAcross: function () {
        var result = "";
        if (this.propertyBuildingName && this.flatUnitShop) {
            result = this.flatUnitShop + ", " + this.propertyBuildingName + ", ";
        }
        else if (this.propertyBuildingName) {
            result = this.propertyBuildingName;
        }
        else if (this.flatUnitShop) {
            result = this.flatUnitShop + "\/";
        }

        if (this.streetNo) {
            result += this.streetNo + " ";
        }

        if (this.street) {
            result += this.street + ", ";
        }

        result += this.suburb;
        return result;
    },
    /*
     First_True
     (
     [Premium - Retirement Village]="Yes"
     ~0


     ~[Premium]>1000000
     ~Truncate
     (
        [Premium]+99
         ~-2
     )-1000000/100*5.5+40490


     ~[Premium]>300000
     ~Truncate
     (
         [Premium]+99
         ~-2
     )-300000/100*4.5+8990


     ~[Premium]>80000
     ~Truncate
     (
         [Premium]+99
         ~-2
     )-80000/100*3.5+1290


     ~[Premium]>30000
     ~Truncate
     (
         [Premium]+99
         ~-2
     )-30000/100*1.75+415


     ~[Premium]>14000
     ~Truncate
     (
         [Premium]+99
         ~-2
     )-14000/100*1.5+175


     ~1=1
     ~Truncate
     (
         [Premium]+99
         ~-2
     )/100*1.25

     )
     */
    premiumDuty: function () {
        ////TODO - Waitting for all the utilities funcitons to become avaliable.
        var result = 0;

        if(this.premiumRetirementVillage === "Yes"){
            return result;
        }
        if(this.premium > 1000000){
            result = "";
        }

        return result;
    },
    /***
     * First_True
     *    (
     *    [{Property}Title Reference]#""
     *   ~[{Property}Property Address (across)]
     *    ~1=1
     *    ~[Property Description - across]
     *    )
     *
     * @returns {string}
     */
    reportPremisesDetails: function () {
        var result = "";
        if(matter.property.titleReference){
            result = matter.property.propertyAddressAcross()
        }
        else{
            result = this.propertyDescriptionAcross();
        }
        return result;
    },
    /**
     *
     * @returns {string}
     */
    sdAssignOrTransferOfLease:function () {
        var result="";
        
        return result;
    },
    /**
     * First_True
     (
     [Term of Lease]#""
     ~[Term of Lease]
     ~1=1
     ~If
     (
     [Term (years)]>0
     ~Format
     (
     [Term (years)]
     ~"Number in Words"
     )+" ("+[Term (years)]+") "+If
     (
     [Term (years)]>1
     ~"Years"
     ~"Year"
     )
     ~""
     )+If
     (
     And
     (
     [Term (days)]>0
     ~[Term (months)]>0
     )
     ~", "
     ~If
     (
     And
     (
     [Term (years)]>0
     ~Or
     (
     [Term (days)]>0
     ~[Term (months)]>0
     )
     )
     ~" and "
     ~""
     )
     )+If
     (
     [Term (months)]>0
     ~Format
     (
     [Term (months)]
     ~"Number in Words"
     )+" ("+[Term (months)]+") "+If
     (
     [Term (months)]>1
     ~"Months"
     ~"Month"
     )
     ~""
     )+If
     (
     And
     (
     [Term (days)]>0
     ~Or
     (
     [Term (years)]>0
     ~[Term (months)]>0
     )
     )
     ~" and "
     ~""
     )+If
     (
     [Term (days)]>0
     ~Format
     (
     [Term (days)]
     ~"Number in Words"
     )+" ("+[Term (days)]+") "+If
     (
     [Term (days)]>1
     ~"Days"
     ~"Day"
     )
     ~""
     )
     )
     */
    termOfLeaseDisplay:function () {
        var result = "";
        if(this.termOfLease){
            result = this.termOfLease;
        }
        else {
            var yearNum = this.termYears();
            var monthsNum = this.termMonths();
            var daysNum = this.termDays();

            var isYear = yearNum > 0;
            var isMonths = monthsNum > 0;
            var isDays = daysNum > 0;

            var isAnd = false;

            if (isYear) {
                result = LEAP.utilities.toWords(yearNum) + " (" + yearNum + ") ";
                if (yearNum > 1) {
                    result += "Years";
                } else {
                    result += "Year";
                }
            }

            if (isDays && isMonths) {
                result += ", ";
            }
            else if(isYear && (isDays || isMonths))
            {
                isAnd = true;
                result += " and ";
            }

            if(isMonths){
                result += LEAP.utilities.toWords(monthsNum) + " (" + monthsNum + ") ";
                if(monthsNum > 1){
                    result += "Months";
                }
                else{
                    result += "Month";
                }
            }
            if(isDays && (isYear || isMonths) && !isAnd){
                result += " and ";
            }
            if(isDays){
                result += LEAP.utilities.toWords(daysNum) + " (" + daysNum + ") ";
                if(daysNum > 1){
                    result += "Days";
                }
                else{
                    result += "Day";
                }
            }
        }
        return result;
    },
    /***
     *
     (
     [{Lease Details}contribution amount]*[{Lease Details}Term (years)]
     )+
     (
     [{Lease Details}contribution amount]/12*[{Lease Details}Term (months)]
     )+
     (
     [{Lease Details}contribution amount]/365.25*[{Lease Details}Term (days)]
     )
     */
    totalContributionAmountFullTerm: function () {
        var result = 0;
        result = this.contributionAmount * this.termYears() +
            this.contributionAmount / 12 * this.termMonths() +
            this.contributionAmount / 365.25 * this.termDays();
        return result.currency();
    },
    /**
     * First_True
     (
         [{Rent}Total Lease Stamp Duty]=0
         ~0

         ~[Duplicate Documents]>0
         ~
         (
         [Duplicate Documents]*2
         )

         ~1=1
         ~2
     )
     */
    totalDuplicateDocumentFees: function () {
        var result = 0;
        result = 2;
        if(matter.rent && matter.rent.totalLeaseStampDuty() === 0) {
            result = 0;
        }
        else if(this.duplicateDocuments > 0){
            result = this.duplicateDocuments * 2
        }
        return result;
    },
    //"Commencement Date: "+[Lease Commencement Date]
    titlelease: function () {
        var result = "";
        result = "Commencement Date: " + this.leaseCommencementDate.toString("dd MMMM, yyyy");
        return result;
    }
}





















